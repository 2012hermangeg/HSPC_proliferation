#############
### Prior ###
#############


struct PriorDistribution <: ContinuousMultivariateDistribution
    """
  Parameter prior distributions
  including binary parameters corresponding to the scenario

  # Arguments:
  - `considered_scenarios::String`: either "all", "main", or "solo"
    If "main": only four main scenarios are considered (H=0 or 1; C=0 or 1) 
    and no concordance between cousin cells (rho_C_cousins = 0).
    If "solo": `sc_H` and `sc_C` must be specified to define scenarios for H and C.

  # Fields:
  - `considered_scenarios::String`: Defines the scenario context.
  - `sc_H::Union{Int, Nothing}`: 
      Scenario value for H, required if `considered_scenarios` is "solo".
  - `sc_C::Union{Int, Nothing}`: 
      Scenario value for C, required if `considered_scenarios` is "solo".
  """
  considered_scenarios::String
  sc_H::Union{Int, Nothing}
  sc_C::Union{Int, Nothing}

  # Default constructor
  PriorDistribution() = new("all", nothing, nothing)

  # Constructor for "all" or "main" scenarios
  function PriorDistribution(considered_scenarios::String)
    if !(considered_scenarios in ["all", "main", "solo"])
      error("Invalid considered_scenarios value: must be 'all', 'main', or 'solo'")
    end
    if considered_scenarios == "solo"
      error("For 'solo' scenario, both sc_H and sc_C must be specified")
    end
    new(considered_scenarios, nothing, nothing)
  end

  # Constructor for "solo" scenario
  function PriorDistribution(considered_scenarios::String, sc_H::Int, sc_C::Int)
    if considered_scenarios != "solo"
      error("sc_H and sc_C can only be specified if considered_scenarios is 'solo'")
    end
    new(considered_scenarios, sc_H, sc_C)
  end
end


# Default constructor
PriorDistribution() = PriorDistribution("all", nothing, nothing)

 

function Distributions.pdf(d::PriorDistribution,  X::AbstractArray{T,1} where {T}=24)
  """
  Evaluate the prior at X
  X is the parameter vector (also called theta)
  
  ###
  For now, the evaluation of the prior is supposed to be done 
  only when studying the four main scenarios.
  This function is indeed called only when usoing the ABC-SMC method
  ###
  """
  (p1_1,#transition probability from type 1 (HSC) to 1 
  p1_2, 
  p1_3,
  mu_1, 
  sig_1,
  rho_H_1,
  p2_2, 
  p2_3, 
  mu_2, 
  sig_2,
  rho_H_2,
  p3_3,
  mu_3, 
  sig_3,
  rho_H_3,
  mu_4, 
  sig_4,
  recovery_rate,
  rho_H_4,
  rho_C_sisters_A,
  rho_C_sisters_B,
  C,
  H ,
  rho_C_cousins
  ) = X

  if d.considered_scenarios != "main"
   println("Warning, prior.pdf implemented only for the 4 main scenarios")
  end
  
  #The prior being uniform over a domain of volume V
  #We return 1/V if we are in the domain, 0 otherwise
  V = 1

  if p1_1 + p1_2 + p1_3 > 1
    return 0.0
  elseif p1_1 > 1 || p1_1 < 0
    return 0.0
  elseif p1_2 > 1 || p1_2 < 0
    return 0.0
  elseif p1_3 > 1 || p1_3 < 0
    return 0.0
  end
  V *= 1/6

  if  p2_2 + p2_3 > 1 
    return 0.0
  elseif p2_2 > 1 || p2_2 < 0
    return 0.0
  elseif p2_3 > 1 || p2_3 < 0
    return 0.0
  end
  V *= 1/2

  if p3_3 > 1 || p3_3 < 0
    return 0.0
  end

  for mu in [mu_1, mu_2, mu_3, mu_4]
    if mu < 1.5 || mu > 4.0
      return 0.0
    end
  end
  V *= (4-1.5)^4

  for sig in [sig_1, sig_2, sig_3, sig_4]
    if sig < 0.0 || sig > 0.5
      return 0.0
    end
  end
  V *= 0.5^4

  if recovery_rate < 0.4 || recovery_rate > 0.9
    return 0.0
  end 
  V*= (0.9-0.4)

  if H == 1
    for rho_H in [rho_H_1, rho_H_2, rho_H_3]
      if rho_H < 0.3 || rho_H > 1.0
        return 0.0
      end
    end
    V *= (1-0.3)
  elseif H == 0 #No homogeneity in fate
    for rho_H in [rho_H_1, rho_H_2, rho_H_3]
      if rho_H != 0.0
        println("error - prior - rho_H")
        return 0.0
      end
    end
  else 
    println("error in the choice of scenario - Homogeneity")
  end

  if C == 1 #Concordance in division
    if rho_C_sisters_A < 0.3 || rho_C_sisters_A > 1.0
      return 0.0
    end
    V *= (1-0.3)
  elseif C == 0 #No concordance
    for rho_C in [rho_C_sisters_A,rho_C_sisters_B]
      if rho_C != 0.0
        println("error - prior - rho_C")
        return 0.0
      end
    end
  end

  if rho_C_cousins != 0.0
    println("problem cousins")
    return 0.0
  end

  return 1/V

end

function Distributions.rand(d::PriorDistribution) 
  """
  Randomly sample a parameter vector theta from the prior
  return:
  theta = (p1_1, 
        p1_2, 
        p1_3,
        mu_1, 
        sig_1,
        rho_H_1,
        p2_2, 
        p2_3, 
        mu_2, 
        sig_2,
        rho_H_2,
        p3_3,
        mu_3, 
        sig_3,
        rho_H_3,
        mu_4, 
        sig_4,
        recovery_rate,
        rho_H_4,
        rho_C_sisters_A,
        rho_C_sisters_B,
        C,
       	H,
        rho_C_cousins
	      )
  """
  ### Transitions from type i to j ###
  # 1 = HSC
  # 2 = MPP
  # 3 = HPC
  # 4 = CD34-
  
  # From HSC
  p1_1 = rand(Uniform(0.0,1))
  p1_2 = rand(Uniform(0.0,1))
  p1_3 = rand(Uniform(0.0,1))
  #p1_4 is implicitly defined as 1 - sum(...)
  while p1_1 + p1_2 + p1_3 > 1 
    p1_1 = rand(Uniform(0.0,1))
    p1_2 = rand(Uniform(0.0,1))
    p1_3 = rand(Uniform(0.0,1))
  end

  # From MPP
  p2_2 = rand(Uniform(0.0,1))
  p2_3 = rand(Uniform(0.0,1))
  while p2_2 + p2_3 > 1 
    p2_2 = rand(Uniform(0.0,1))
    p2_3 = rand(Uniform(0.0,1))
  end


  # From CD34-
  p3_3 = rand(Uniform(0.0,1))

  ### Parameters related to the divisions (except the first one) ###
  # where the division times follow a (Mv)LogNormal law
  # of parameters mu and sig
  mu_1 = rand(Uniform(1.5,4.0))
  sig_1 = rand(Uniform(0.0,0.5))

  mu_2 = rand(Uniform(1.5,4.0))
  sig_2 = rand(Uniform(0.0,0.5))


  mu_3 = rand(Uniform(1.5,4.0))
  sig_3 = rand(Uniform(0.0,0.5))

  mu_4 = rand(Uniform(1.5,4.0))
  sig_4 = rand(Uniform(0.0,0.5))

  #Recovery rate
  recovery_rate = rand(Uniform(0.4, 0.9))
		     
  ### Homogeneity in fate ###
  rho_H_1 = 0.0
  rho_H_2 = 0.0
  rho_H_3 = 0.0
  rho_H_4 = 0.0 #Since CD34- cells can only becom CD34-, this parameters is not used

  H = rand(collect(0:2))
  if d.considered_scenarios == "main"
    H = rand(collect(0:1))
  elseif d.considered_scenarios == "solo"
    H = d.sc_H
  end
  #H == 0 corresponds to the cas without homogeneity in fate, i.e, rho_H_i = 0

  if H == 1
    #Same homogeneity between all sister cells
    rho_H_1 = rand(Uniform(0.3,1.0))
    rho_H_2 = rho_H_1
    rho_H_3 = rho_H_1

  elseif H == 2
    #Different homogeneity according to the cell type
    rho_H_1 = rand(Uniform(0.0,1.0))
    rho_H_2 = rand(Uniform(0.0,1.0))
    rho_H_3 = rand(Uniform(0.0,1.0))
  
    while (abs(rho_H_1-rho_H_2) + abs(rho_H_1-rho_H_3) + abs(rho_H_2-rho_H_3) < 0.3) && (rho_H_1 + rho_H_2 + rho_H_3 < 0.3)
      #Ensure that this scenario differs sufficiently from the previous one
      rho_H_1 = rand(Uniform(0.0,1.0))
      rho_H_2 = rand(Uniform(0.0,1.0))
      rho_H_3 = rand(Uniform(0.0,1.0))
    end
  end


  ### Concordance in division ###
  rho_C_sisters_A = rand(Uniform(0.3 ,1.0))
  rho_C_sisters_B = rand(Uniform(0.0,1.0))

  while abs(rho_C_sisters_B-rho_C_sisters_A) < 0.2
    #To ensure being not in the case where we have two almost identical values
    rho_C_sisters_B = rand(Uniform(0.0,1.0))
  end

  C = rand(collect(0:2))
  if d.considered_scenarios == "main"
    C = rand(collect(0:1))
    if C == 0
      rho_C_sisters_A == 0.0
    end
    rho_C_sisters_B = 0.0
  end

  # 0 -> no concordance would be considered 
  #(rho_C_sisters_A and B useless during the simu)
  # 1 -> Same concordance (rho_C_sisters_A & B identical during the simu)
  # 2 -> different concordance according to criteria, as specified in construct_cov_mat
  
  if C == 2
    #Sub-scenarios are considered
    C = rand(collect(2:5))
  end

  if d.considered_scenarios == "solo"
    C = d.sc_C
  end

  rho_C_cousins = rand(Beta(0.5,0.5))
  if d.considered_scenarios == "main"
    rho_C_cousins = 0.0
  end

  return (p1_1 = p1_1, 
        p1_2 = p1_2, 
        p1_3 = p1_3,
        mu_1 = mu_1, 
        sig_1 = sig_1,
        rho_H_1 = rho_H_1,
        p2_2 = p2_2, 
        p2_3 = p2_3, 
        mu_2 = mu_2, 
        sig_2 = sig_2,
        rho_H_2 = rho_H_2,
        p3_3 = p3_3,
        mu_3 = mu_3, 
        sig_3 = sig_3,
        rho_H_3 = rho_H_3,
        mu_4 = mu_4, 
        sig_4 = sig_4,
        recovery_rate = recovery_rate,
        rho_H_4 = rho_H_4,
        rho_C_sisters_A = rho_C_sisters_A,
        rho_C_sisters_B = rho_C_sisters_B,
        C = C,
      	H = H,
        rho_C_cousins = rho_C_cousins
      	)
end

function get_labels_prior(d::PriorDistribution) 
  dummy_sample = rand(d)
  return keys(dummy_sample)
end

