# Observation Model

## Introduction

The `observation_model.jl` file provides Julia functions to simulate observational processes and apply post-simulation treatments to the data generated by cell proliferation models. These functions mimic real-world experimental limitations, such as censorship of division times and sampling noise.

## Features

- **Censorship**: Applies interval censorship to division times to reflect the limits of observation windows in Live Cell Imaging assays.
- **Sampling Noise**: Simulates the loss of cells during observation due to sampling inefficiencies, modeled as a binomial process.

## Functions

### `censor_Incucyte`

This function applies interval censorship to a list of continuous division times based on the observation time and desired censorship type.

- **Parameters**:
  - `continous_times`: A vector or matrix of division times.
  - `observation_time`: The maximum time until which the simulation runs.
  - `censor`: The type of censorship to apply, `:left` for interval-censored left or `:right` for interval-censored right.

### `sampling_noise_MG`

Simulates sampling noise on the observations from the MultiGen assay, representing the loss of cells with a specified recovery rate.

- **Parameters**:
  - `mat_well`: A matrix representing the cell counts in a well.
  - `recovery_rate`: The probability of a cell being successfully observed (default is 0.7).

## Usage

These functions are essential for researchers aiming to incorporate realistic observational constraints into simulations of cell proliferation. By accounting for experimental limitations, the resulting data more accurately reflects what might be observed in practical assays.


# Prior 
## Introduction

The `PriorDistribution` struct in this code defines a framework for setting up prior distributions for parameters in a continuous multivariate distribution model. It specifically supports scenario-based modeling, where the inclusion of binary parameters allows for scenario-specific computations.

## Structure Definition

The `PriorDistribution` struct extends `ContinuousMultivariateDistribution` and includes:
- **`considered_scenarios`**: A string parameter that specifies the scenario context for the distribution ("all" or "main").

### Constructors

- Default Constructor: Initializes `considered_scenarios` to `"all"` unless specified otherwise. This is suitable when all scenarios are under consideration.
- Custom Constructor: Allows explicit specification of scenarios to focus on a subset, such as the main scenarios only.

## Functions

### `pdf`

Calculates the probability density function (PDF) of the distribution at a given parameter vector (`X`). This function is tailored to evaluate the prior only under the specified scenarios, primarily used with the ABC-SMC method for scenario-specific studies.

- **Parameters**: 
  - `X`: The parameter vector, commonly denoted as theta in statistical models.

### `rand`

Generates a random sample from the prior distribution, respecting the defined scenarios and their constraints. This function is essential for stochastic simulations where parameters are randomly drawn following the specified prior distributions.

### `get_labels_prior`

Extracts and returns the labels of the parameters from a sampled instance of the prior distribution. This utility function is helpful for dynamically accessing parameter names in a structured format.


# Computation of the Summary Statistics

## Introduction

The `summary_stats.jl` file provides a suite of functions designed to compute summary statistics for cell proliferation data obtained from Live Cell Imaging and MultiGen assays. These functions handle data preprocessing, including censoring and statistical analysis, to facilitate the interpretation of experimental outcomes.

## Functions Overview

### `compute_ss_incucyte`

Calculates summary statistics from data matrices associated with the Incucyte assay (Live Cell Imaging).

- **Parameters:**
  - `mat_incucyte`: Matrix of division times per well.
  - `obs_time`: Observation time (default is 96 hours).
  - `nb_wells_used_for_computation`: Number of wells considered in the computation (default is Inf, indicating all wells).

### `compute_ss_colony_size`

Computes statistics related to colony sizes at the end of the observation period.

- **Parameters:**
  - `vect_colony_size`: Vector containing the count of cells per well at the observation time.
  - `nb_wells_used_for_computation`: Number of wells considered in the computation (default is Inf).

### `compute_ss_MG`

Generates summary statistics from MultiGen assay data.

- **Parameters:**
  - `M`: Matrix where each row represents a well, and columns correspond to concatenated generations and cell types.
  - `nb_gen_max`: Maximum number of generations (default is 7).
  - `nb_cell_types`: Number of cell types considered (default is 4).
  - `nb_wells_used_for_computation`: Number of wells considered in the computation (default is Inf).

### `get_cols_gen`

Utility function to retrieve column indices corresponding to a specific generation from reshaped MultiGen data.

- **Parameters:**
  - `gen`: Specific generation number.
  - `nb_gen_max`: Maximum number of generations.
  - `nb_cell_types`: Number of cell types.

### `get_labels_sum_stats`

Extracts the labels of summary statistics from computed data, useful for labeling output in reports or further analyses.

- **Parameters:**
  - `exp_MG`: Experimental configurations for the MultiGen assay.
  - `exp_Inc`: Experimental configurations for the Incucyte assay.
  - `nb_gen_max`: Maximum number of generations.
  - `nb_cell_types`: Number of cell types.
  - `map_cell_types`: Mapping of numerical indices to cell type names.
