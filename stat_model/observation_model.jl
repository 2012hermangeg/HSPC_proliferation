#########################
### Observation model ###
#########################

function censor_Incucyte(continous_times::Union{Matrix{Float64}, Vector{Float64}},
		         observation_time::Union{Float64,Int64};
			       censor = :left)
  """
  Apply an interval censorship to the data simulated by the Live Cell Imaging (Incucyte) assay

  # Arguments required:
  - `continous_times::Union{Matrix{Float64}, Vector{Float64}}`: List of division times
  - ` observation_time::Union{Float64,Int64}`: Maximal time until which the simulation is run [in hours]

  # Optionnal arguments:
  - `censor`: by default, :left. Other choice: :right
  """
  
  #First, we sort T2_1 and T2_2 such that T2_1 <= T2_2
  for i in 1:size(continous_times,1)
    T2_1 = continous_times[i,2]
    T2_2 = continous_times[i,3]
    if T2_1 > T2_2
      continous_times[i,2] = T2_2
      continous_times[i,3] = T2_1
    end
  end

  if censor == :left
    # Interval-censored left
    # As for the real observations (after pretraiting them)
    # 2.3 h becomes 2 h (Int)
    # Inf become observation_time
    return  floor.(Int64, min.(continous_times, observation_time))
		
  elseif censor == :right
    # Inf (float64) remains Inf
    # We return a Float vector
    return ceil.(continous_times)

  else
    println("error: censor not defined")
    return continuous_time

  end

end


function sampling_noise_MG(mat_well::Matrix{Int64};
			  recovery_rate = 0.7,
			  )
  """
  Return the MultiGen observations
  where some cells are lost
  due to a sampling noise
  """

  for i in 1:size(mat_well,1)
    for j in 1:size(mat_well,2)
      if mat_well[i,j] != 0
        mat_well[i,j] = rand(Binomial(mat_well[i,j],recovery_rate))
       end
     end
  end
   
  return mat_well

end

