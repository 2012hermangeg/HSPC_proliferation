##########################
### Summary statistics ###
##########################

####### Related to the Live Cell Imaging (Incucyte) Assay #########

function compute_ss_incucyte(mat_incucyte::Union{Matrix{Float64}, Matrix{Int64}};
			    obs_time=96,
			    nb_wells_used_for_computation=Inf
			    )
  """
  Compute the summary statistics (ss)
  related to the Incucyte assay (Live Cell Imaging)
  For a given (here not specified) and same initial cell type

  The (left) censoring has to be applied first,
  as the sorting step between T2_1 and T2_2 so that T2_1 < T2_2

  If the division time are left censored,
  then, each division equal to the obs_time is in fact unobserved
  (see censor_incucyte in observation_model.jl)

  # required Argument:
  - `(mat_incucyte::Union{Matrix{Float64}, Matrix{Int64}}`: input matrix associated to the Incucyte Assay
  each line corresponds to a well (or, equivalently, to a familly)
  first column corresponds to the first division time (left-interval censored)
  second and third columns: T2_1 and T2_2 so that T2_1 <= T2_2

  # Optionnal argument:
  - `obs_time`: observation time (by default equal to 96 hours)

  # Return:
  - an array (NamedTuple) with several summary statistics computed over the different wells

  """

  if nb_wells_used_for_computation < size(mat_incucyte,1)
    if nb_wells_used_for_computation == 0
      return []
    end
    mat_incucyte = mat_incucyte[1:nb_wells_used_for_computation,:]
  end

  T1 = mat_incucyte[:,1]
  T2_1 = mat_incucyte[:,2]
  T2_2 = mat_incucyte[:,3] #T2_1 has to be <= T2_2
  T3 = mat_incucyte[:,4:7]

  #If we exclude unobserved values (superior or equal to obs_time if there is a left censor)
  T1_obs = T1[T1 .< obs_time] #Left (interval) censoring
  T2_1_obs = T2_1[T2_1 .< obs_time]
  T2_2_obs = T2_2[T2_2 .< obs_time]
	
  #Mean second division time
  T2 = 0.5 .*(T2_2_obs .+ T2_1[T2_2 .< obs_time])

  #Compute sum. stats. related to T3
  min_T3 = minimum(T3, dims=2)[:,1]
  T3_all_obs = T3[min_T3.<obs_time,:]
  T2_when_T3 = 0.5 .*(T2_2[min_T3.<obs_time,1] .+ T2_1[min_T3.<obs_time,1])

  diff_T3_T2 = Inf
  std_diff_T3_T2 = Inf

  if size(T3_all_obs,1) > 1
    diff_T3_T2 = mean(T3_all_obs .- T2_when_T3, dims=2)
    std_diff_T3_T2 = std(T3_all_obs .- T2_when_T3, dims=2)
  end

  return (
    prop_observed_T1 = length(T1_obs)/length(T1),
	  prop_observed_T2_1 = length(T2_1_obs)/length(T2_1),
	  prop_observed_T2_2 = length(T2_2_obs)/length(T2_2),
	  median_T2_1 = median(T2_1), 
	  median_T2_2 = median(T2_2), 
    mean_T2_1 = mean(T2_1_obs), 
	  std_T2_1 = std(T2_1_obs), 
    skewness_T2_1 = mean(((T2_1_obs.-mean(T2_1_obs))./std(T2_1_obs)).^3),
	  mean_T2_2 = mean(T2_2_obs), 
	  std_T2_2 = std(T2_2_obs), 
    skewness_T2_2 = mean(((T2_2_obs.-mean(T2_2_obs))./std(T2_2_obs)).^3),
	  mean_T2 = mean(T2), 
	  std_T2 = std(T2), 
    skewness_T2 = mean(((T2.-mean(T2))./std(T2)).^3),
    cor_T2_sisters = cor(T2_1[T2_2.<obs_time],T2_2_obs),
    mean_diff_T2_1_T1 = mean(T2_1_obs.-T1[T2_1.<obs_time]), 
	  std_diff_T2_1_T1 = std(T2_1_obs.-T1[T2_1.<obs_time]), 
    mean_diff_T2_2_T1 = mean(T2_2_obs.-T1[T2_2.<obs_time]), 
	  std_diff_T2_2_T1 = std(T2_2_obs.-T1[T2_2.<obs_time]), 
    corr_diff_sisters = cor(T2_1[T2_2.<obs_time].-T1[T2_2.<obs_time],T2_2_obs.-T1[T2_2.<obs_time]),
	  mean_diff_T3_T2 = mean(diff_T3_T2),
	  mean_std_diff_T3_T2 = mean(std_diff_T3_T2)
	  )

end

function compute_ss_colony_size(vect_colony_size::Vector{Int64};
				nb_wells_used_for_computation=Inf)
  """
  Compute summary statistics associated to the count of cells at the observation time
  The count of cells (also called colony size) is calculated in the incucyte assay (Inc)
  """

  if nb_wells_used_for_computation < length(vect_colony_size)
    if nb_wells_used_for_computation == 0
      return []
    end
    vect_colony_size = vect_colony_size[1:nb_wells_used_for_computation]
  end

  return (
    mean_nb_cells_Inc = mean(vect_colony_size),
	  median_nb_cells_Inc = median(vect_colony_size),
	  std_nb_cells_Inc = std(vect_colony_size)
	  )

end


####### Related to theMultiGen Assay #########

function get_cols_gen(gen, nb_gen_max, nb_cell_types)
  """
  If the Multy Gen Assay output (for one well) has been reshaped
  such that it corresponds to a vector with nb_gen_max x nb_cell_types elements
  rather than a matrix of nb_gen_max lines and nb_cell_types columns
  Then we return the indexes of the vector which correspond to the generation gen
  (for the matrix, they would correspond to line gen)
  """
  return [nb_gen_max*(i-1)+gen for i in 1:nb_cell_types]
end

function compute_ss_MG(M::Matrix{Int64};
		      nb_gen_max = 7,
		      nb_cell_types = 4,
		      nb_wells_used_for_computation=Inf)

  """
  Compute the summary statistics (ss) associated to the MultiGen (MG) assay

  # Arguments
  - `M::Matrix{Int64}`: Each line is a vector corresponding to the ith well
  This vector `v` has a length of nb_gen_max x nb_cells_types
  At the ith position (pos) in this vector, corresponds:
    pos % nb_gen_max corresponds to the generation (with 0 -> gen_max)
    pos % nb_cell_types corresponds to the cell_type

  reshape(`v`, (nb_gen_max,nb_cell_types)) gives the MG Matrix as provided after simulating a well
  """

  if nb_wells_used_for_computation < size(M,1)
    if nb_wells_used_for_computation == 0
      return []
    end
    M = M[1:nb_wells_used_for_computation,:]
  end

  nb_wells = size(M,1) #Also nb of families (or colonies)
  
  if size(M,2) != nb_gen_max*nb_cell_types
    println("error: there should be nb_gen_max x nb_cell_types elements")
  end

  nb_total_cells = sum(M, dims=2)
  mean_nb_cells = mean(nb_total_cells)
  median_nb_cells = median(nb_total_cells)
  std_nb_cells = std(nb_total_cells)

  # effective nb of cells correspond to the ponderated sum of cells
  # By "penalizing" higher generation
  # To account for proliferation
  # i.e., the weight is 1/gen (gen 1 corresponds to the initial cell)
  nb_cells_effective = zeros(nb_wells)

  ### Sum. Stats. related to the cell generation ###
  nb_cells_gen_i = zeros(nb_wells, nb_gen_max) #Nb of cells in gen i per well
  coverage_gen = zeros(nb_wells) #will correspond to the nb of generations we have per well

  # Sum. stats associated to a particular generation (index i)
  mean_nb_cells_gen_i = zeros(nb_gen_max) 
  std_nb_cells_gen_i = zeros(nb_gen_max)
  prop_families_only_in_gen_i = zeros(nb_gen_max)

  for i in 1:nb_gen_max
    nb_cells_gen_i[:,i] = sum(M[:,get_cols_gen(i, nb_gen_max, nb_cell_types)],dims=2) #We sum over the cell types, since a cell in gen i can be either type 1, type 2, etc.

    nb_cells_effective .+= nb_cells_gen_i[:,i]/i

    coverage_gen .+= nb_cells_gen_i[:,i] .> 0

    # Compute sum. stat. per gen i
    mean_nb_cells_gen_i[i] = mean(nb_cells_gen_i[:,i])
    std_nb_cells_gen_i[i] = std(nb_cells_gen_i[:,i])
    prop_families_only_in_gen_i[i] = mean(nb_cells_gen_i[:,i].==nb_total_cells)

  end

  prop_fam_over_n_gen = zeros(nb_gen_max)
  for n in 1:nb_gen_max
    prop_fam_over_n_gen[n] = mean(coverage_gen.==n)
  end

  mean_coverage_gen = mean(coverage_gen)
  std_coverage_gen = std(coverage_gen)

  mean_nb_cells_eff = mean(nb_cells_effective)
  std_nb_cells_eff = std(nb_cells_effective)

  ### Sum. Stats. related to the cell type ###
  nb_cells_of_type_i =  zeros(nb_wells, nb_cell_types)
  coverage_type = zeros(nb_wells)

  mean_nb_cells_type_i = zeros(nb_cell_types)
  std_nb_cells_type_i = zeros(nb_cell_types)
  prop_families_only_in_type_i = zeros(nb_cell_types)

  for i in 1:nb_cell_types
    nb_cells_of_type_i[:,i] = sum(M[:, nb_gen_max*(i-1)+1:nb_gen_max*i],dims=2)

    coverage_type .+= nb_cells_of_type_i[:,i] .> 0

    mean_nb_cells_type_i[i] = mean(nb_cells_of_type_i[:,i])
    std_nb_cells_type_i[i] = std(nb_cells_of_type_i[:,i])
    prop_families_only_in_type_i[i] = mean(nb_cells_of_type_i[:,i].==nb_total_cells)

  end


  prop_fam_over_n_types = zeros(nb_cell_types)
  for n in 1:nb_cell_types
    prop_fam_over_n_types[n] = mean(coverage_type.==n)
  end

  mean_coverage_type = mean(coverage_type)
  std_coverage_type = std(coverage_type)

  return (
	  mean_nb_cells = mean_nb_cells, #Float64
	  median_nb_cells = median_nb_cells, #Float64
	  std_nb_cells = std_nb_cells, #Float64
	  #
	  mean_nb_cells_gen_i = mean_nb_cells_gen_i, #vect. of size nb_gen_max
	  std_nb_cells_gen_i = std_nb_cells_gen_i, #vect. of size nb_gen_max
	  prop_families_only_in_gen_i = prop_families_only_in_gen_i, #vect. of size nb_gen_max
	  #
	  prop_fam_over_n_gen = prop_fam_over_n_gen, #vect. of size nb_gen_max
	  mean_coverage_gen = mean_coverage_gen, #Float64
	  std_coverage_gen = std_coverage_gen, #Float64
	  #
	  mean_nb_cells_eff = mean_nb_cells_eff, #Float64
	  std_nb_cells_eff = std_nb_cells_eff ,#Float64
          #
	  mean_nb_cells_type_i = mean_nb_cells_type_i, #vect. of size nb_cell_types
	  std_nb_cells_type_i = std_nb_cells_type_i, #vect. of size nb_cell_types
	  prop_families_only_in_type_i = prop_families_only_in_type_i, #vect. of size nb_cell_types
	  #
	  prop_fam_over_n_types = prop_fam_over_n_types, #vect. of size nb_cell_types
	  mean_coverage_type = mean_coverage_type, #Float64
	  std_coverage_type = std_coverage_type #Float64
	  )

end


function get_labels_sum_stats(exp_MG::Vector{Any},
			     exp_Inc::Vector{Any};
			     nb_gen_max=7,
			     nb_cell_types=4,
			     map_cell_types = ["HSC", "MPP", "HPC"]
			     )

  ### Assocatied with the MG assay,###
  # MG experiments can be conducted both for 
  # - different initial cell types
  # - different observation times
  dummy_M_MG = zeros(Int64,2, nb_gen_max*nb_cell_types)
  ss_MG_dummy = compute_ss_MG(dummy_M_MG,
			      nb_gen_max=nb_gen_max, 
			      nb_cell_types=nb_cell_types)  


  labels_MG = []
  for k in keys(ss_MG_dummy)
    v = ss_MG_dummy[k]
    if typeof(v) == Float64
      push!(labels_MG, k)
    else
      #Then it is a vector / array
      if length(v) == nb_gen_max
      	for g in 1:nb_gen_max
	        push!(labels_MG, Symbol(string(k,"_",g)))
       	end
      elseif length(v) == nb_cell_types
      	for i in 1:nb_cell_types
      	  push!(labels_MG, Symbol(string(k,"_",i)))
      	end
      else
      	println("error: situation not taken into account")
      end
    end
  end

  full_labels_MG = []
  for i in 1:length(exp_MG)
    initial_cell = exp_MG[i][1]
    obs_time = exp_MG[i][2]
    full_labels_MG = vcat(full_labels_MG,
			   string.(labels_MG, 
				 string("_start" ,map_cell_types[initial_cell],"_obs",obs_time))
			 )
  end

  ### Associated with the Incucyte assay (Live Cell Imaging)
  # For which we can have different initial cell types

  ss_dummy_col_size = compute_ss_colony_size(zeros(Int64,2))
  ss_dummy_inc = compute_ss_incucyte(zeros(Int64,2,7))
  labels_inc = []
  for v in keys(ss_dummy_inc)
    push!(labels_inc, v)
  end
  for v in keys(ss_dummy_col_size)
    push!(labels_inc, v)
  end


  full_labels_Inc = []
  for i in 1:length(exp_Inc)
    initial_cell = exp_Inc[i]
    #There should only be observations at 96h for the Incucyte assay
    full_labels_Inc = vcat(full_labels_Inc, 
			    string.(labels_inc, 
				    string("_start",map_cell_types[initial_cell]))
			   )
  end

  return vcat(full_labels_Inc, full_labels_MG)
  
end

