###########################
### Visualisation tools ###
###########################

function construct_heatmap_MG(mat_MG::Union{Array{Any},Vector{Matrix{Int64}}};
			    nb_lines_max=60,
			    shuffle_lines=true
			    )
  """
  Construct a heatmap correspond to the MultIgen observations
  at the observation time
  Each line is a familly
  """
  nb_lines = nb_lines_max

  M = deepcopy(mat_MG)
  if length(M) > nb_lines_max
    if shuffle_lines 
      #Sample randomly some families to display them
      M = shuffle!(M)[1:nb_lines_max] 
    else
      #More appropriate for displaying true (i.e., not simulated) data
      M = M[1:nb_lines_max]
    end
  else
    nb_lines = length(M)
  end

  nb_gen_max = size(M[1],1)
  nb_cell_types = size(M[1],2)


  nb_col_max = 0
  ind_starting_col_gen = [1] #Gives, for gen i, where it starts in the heatmap
   
  for i in 1:nb_gen_max
    nb_col_max += 2^(i-1)
    push!(ind_starting_col_gen, nb_col_max + 1)
  end


  heatmap_full = zeros(Int64,nb_lines,nb_col_max)

  #We will count the max number of cells within a generation
  #To display a sparser heatmap
  #But we still want at least one column per gen, even if empty
  max_cell_per_gen = zeros(Int64, nb_gen_max) .+ 1

  for i in 1:length(M)
    mat_well = M[i]
    for g in 1:nb_gen_max
      local_ind = 0
      for t in 1:nb_cell_types
      	for n in 1:mat_well[g,t]
	        pos = ind_starting_col_gen[g]+local_ind
	        if pos > nb_col_max
	          #Possible since cells with a higher generation
	          #Would be associated to the maximal observable gen
	          #We keep track of such (unlikely) behavior
	          heatmap_full[i,nb_col_max] = nb_cell_types + 1
	        else
	          heatmap_full[i,pos] = t
	          local_ind += 1
	        end
      	end
      end
      max_cell_per_gen[g] = max(max_cell_per_gen[g], local_ind)
    end
  end

  heatmap_sparse = heatmap_full[:,1]
  effective_gen_end = [1]
  for g in 2:nb_gen_max
    heatmap_sparse = hcat(heatmap_sparse, 
			  heatmap_full[:,ind_starting_col_gen[g]:ind_starting_col_gen[g]+max_cell_per_gen[g]-1])
    push!(effective_gen_end, size(heatmap_sparse, 2))
  end

  return (heatmap_sparse, effective_gen_end)

end

function construct_heatmap_MG(mat_MG::Matrix{Int64};
				nb_gen_max=7,
				nb_cell_types=4,
			  nb_lines_max=60,
			  shuffle_lines=true
			    )
  if size(mat_MG,2) != nb_gen_max*nb_cell_types
    println("error: dims(mat,2) should equal nb_gen_max x nb_cell_types")
  else
    mat_reformated = [reshape(mat_MG[i,:],(nb_gen_max, nb_cell_types)) 
		      for i in 1:size(mat_MG,1)]

    construct_heatmap_MG(mat_reformated,
			    nb_lines_max=nb_lines_max,
			    shuffle_lines=shuffle_lines
			    )
    end
end



function plot_heatmap(heatmap_sparse::Matrix{Int64},
		      effective_gen_end::Vector{Int64};
		      title = "",
		      xlabel = "Generation", 
		      ylabel="Family id",
		      plot_size = (400,600),
		      list_colors = [:green, :lightseagreen, :orange, :darkblue, :black]
		      )
  """
  Plot heatmap
  """

  nb_lines = size(heatmap_sparse, 1)

  nb_col = size(heatmap_sparse, 2)
  nb_gen = length(effective_gen_end)


  fig = plot(title=title, xlabel=xlabel, size=plot_size,
	     ylabel=ylabel,
	    grid=false)

  #Horizontal separation (family)
  for i in 1:nb_lines
    hline!([i], linewidth=2, linestyle=:dot, color=:lightgrey, label="")
  end

  #Display cells
  #Position: x correponds to the gen, y to the familly
  #Color is associated to a cll type
  for line in 1:nb_lines
    for col in 1:nb_col
      cell_type = heatmap_sparse[line, col] 
      if cell_type > 0 #Otherwise, there is no cell
         scatter!([col-0.5], [line-0.5],
		       color=list_colors[cell_type],
		       marker=:square, 
		       markersize=2,
		       markerstrokewidth=0,
		       label="")
      end
    end
  end

  #Vertical separation (Generations)  
  for g in 1:nb_gen
    vline!([effective_gen_end[g]],
	  linewidth=2, color=:lightgrey, label="", linestyle=:dot,
	   )
  end

  pos_xticks = [0.5]
  for i in 2:nb_gen
    push!(pos_xticks, 
	  effective_gen_end[i-1]+0.5*(effective_gen_end[i]-effective_gen_end[i-1])
	  )
  end

  plot!(xticks=(pos_xticks, string.(collect(1:nb_gen))),
	    yticks=(collect(1:nb_lines).-0.5, string.(collect(1:nb_lines))),
	    xlims=(-0.5, Inf), 
	    ylims=(0,Inf),
	    left_margin=5Plots.mm
       )

  return fig
end
  

function construct_binary_tree(p::Vector{Int64},
		m::Vector{Int64},
		TA::Vector{Float64},
		observation_time::Float64
		)

  
  horizontal_line_set = []
  vertical_line_set = []

  rec_tree(p,
	     m,
	     TA,
	     1, #Index of the initial cell
	     1, #Level of the initial cell
	     0.0, #y-coordinate of the initial cell
	     TA[1], # equal to 0.0
	     p[1], #Initial cell type
	     observation_time,
	     horizontal_line_set,
	     vertical_line_set
	     )

  return (horizontal_line_set,vertical_line_set)



end

function rec_tree(p::Vector{Int64},
		m::Vector{Int64},
		TA::Vector{Float64},
		mother::Int64,
		level_mother::Int64,
		y_mother::Float64,
		TA_mother::Float64,
		type_mother::Int64,
		observation_time::Float64,
		horizontal_line_set::Array{Any},
		vertical_line_set::Array{Any}
		)

  daughter1 = 0
  daughter2 = 0
  for i in 2:2:length(m)
    if m[i] == mother
      daughter1 = i
      daughter2 = i+1
      break
    end
  end

  if daughter1 == 0
    #mother has not divided before the end of the exp
    push!(horizontal_line_set, 
	  (y_mother, TA_mother, observation_time, type_mother)
	  )

  else
    TA_daughter = TA[daughter1]

    #Horizontal line
    push!(horizontal_line_set, 
	  (y_mother, TA_mother, TA_daughter, type_mother)
	  )

    #Daughter 1 - vertical line 1
    y_daughter1 = y_mother + 1/(2^level_mother)
    type_daughter1 = p[daughter1]

    push!(vertical_line_set, 
	  (TA_daughter, y_mother, y_daughter1, type_daughter1)
	  )

    rec_tree(p,
	     m,
	     TA,
	     daughter1,
	     level_mother+1,
	     y_daughter1,
	     TA_daughter,
	     type_daughter1,
	     observation_time,
	     horizontal_line_set,
	     vertical_line_set
	     )

    #Daughter 2 - vertical line 1
    y_daughter2 = y_mother - 1/(2^level_mother)

    type_daughter2 = p[daughter2]

    push!(vertical_line_set, 
	  (TA_daughter, y_mother, y_daughter2, type_daughter2)
	  )

    rec_tree(p,
	     m,
	     TA,
	     daughter2,
	     level_mother+1,
	     y_daughter2,
	     TA_daughter,
	     type_daughter2,
	     observation_time,
	     horizontal_line_set,
	     vertical_line_set
	     )

  end
end

function plot_binary_tree(horizontal_line_set::Array{Any},
			 vertical_line_set::Array{Any};
			 title = "",
			 xlabel = "Time [h]", 
			  plot_size = (400,400),
			  list_colors = [:green, :lightseagreen, :orange, :darkblue]
			 )

  fig = plot(title=title, xlabel=xlabel, size=plot_size,
	     ylabel="",
	    grid=false)

  observation_time = 0.0

  for i in 1:length(horizontal_line_set)
    l = horizontal_line_set[i]

    plot!([l[2], l[3]], [1,1].*l[1], 
	  linestyle=:solid,
	  linewidth=2, 
	  color=list_colors[l[4]],
	  label=""
	  )

    observation_time = max(observation_time,  l[3])
  end

  for i in 1:length(vertical_line_set)
    l = vertical_line_set[i]

    plot!( [1,1].*l[1], [l[2], l[3]],
	  linestyle=:solid,
	  linewidth=2, 
	  color=list_colors[l[4]],
	  label=""
	  )
  end

  vline!([observation_time], color=:grey, linestyle=:dash, label="")

  plot!(yaxis=false)
  
  return fig

end

