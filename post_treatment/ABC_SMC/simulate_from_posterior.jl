###############################################
### Simulate from the posterior
### and compute summary statistics 
###############################################


function simulate_from_posterior(df_posterior, T_inc)
  """
  simulate and return a dataframe
  with as many lines as in the input dataframe
  For each line, we have a parameter vector sampled from the posterior 
  and the associated computed summary statistics

  df_posterior corresponds to a dataframe where the 24 first columns correspond to the parameters
  """

  N = size(df_posterior, 1)
  ### Some general settings ###
  nb_cell_types = 4;
  nb_gen_max = 7;

  # Experimental configurations 
  # Array of quadruplet:
  # (initial cell type, obs time, nb of wells for Inc, nb of wells for MG)
  configs = [
	  (1, 72, 0, 135) 
	  (2, 72, 0, 167) 
	  (1, 96, 152, 271) #From HSC
	  (2, 96, 361, 261) #From MPP
	  (3, 96, 146, 247) #From HPC
	  ]
  map_cell_types = ["HSC", "MPP", "HPC"]
 
  ### First division time ###
  #T incucyte - first div - cocktail Diff - Bone Marrow

  T1_HSC = T_inc[T_inc[:,:initial].=="HSC",:T1]
  T1_MPP = T_inc[T_inc[:,:initial].=="MPP",:T1]
  T1_HPC = T_inc[T_inc[:,:initial].=="HPC",:T1]

  empirical_distribution_T1 = [T1_HSC, T1_MPP, T1_HPC]


  ### Prepare to store the results ###
  # Initialize a DataFrame
  df = DataFrame()

  # First colums for parameters
  label_parameters = get_labels_prior(PriorDistribution("all"))

  for label in label_parameters
    df[!, label] = zeros(Float64, N)
  end

  exp_MG = [] #MG - MultiGen
  exp_Inc = [] # Inc - Incucyte (Live Cell Imaging)
  for i in 1:length(configs)
    if configs[i][3] > 0 #obs for Inc
      push!(exp_Inc, configs[i][1])
    end
    if configs[i][4] > 0 #obs for MG
      push!(exp_MG, (configs[i][1],configs[i][2]))
    end
  end

  # Then, the colums associated to the summary statistics
  label_ss = get_labels_sum_stats(
		    exp_MG, #Diff config MG
		    exp_Inc, #Diff configs Inc
		    nb_gen_max = nb_gen_max,
		    nb_cell_types = nb_cell_types,
		    map_cell_types = map_cell_types
				)

  for label in label_ss
    df[!, label] = zeros(Float64, N)
  end


  ##################
  ### Simulation ###
  ##################

  println()
  println("### Begin computation ###")
  println(string("with N = ",N))
  println()

  @time for i in 1:N
  
    theta = (p1_1 =  df_posterior[i,:p1_1], 
        p1_2 =  df_posterior[i,:p1_2], 
        p1_3 =   df_posterior[i,:p1_3],
        mu_1 =  df_posterior[i,:mu_1], 
        sig_1 =   df_posterior[i,:sig_1],
        rho_H_1 =   df_posterior[i,:rho_H_1],
        p2_2 =  df_posterior[i,:p2_2], 
        p2_3 =   df_posterior[i,:p2_3], 
        mu_2 =  df_posterior[i,:mu_2], 
        sig_2 =   df_posterior[i,:sig_2],
        rho_H_2 =   df_posterior[i,:rho_H_2],
        p3_3 =  df_posterior[i,:p3_3],
        mu_3 =  df_posterior[i,:mu_3], 
        sig_3 =   df_posterior[i,:sig_3],
        rho_H_3 =  df_posterior[i,:rho_H_3],
        mu_4 =  df_posterior[i,:mu_4], 
        sig_4 =   df_posterior[i,:sig_4],
        recovery_rate =   df_posterior[i,:recovery_rate],
        rho_H_4 =   df_posterior[i,:rho_H_4],
        rho_C_sisters_A =  df_posterior[i,:rho_C_sisters_A],
        rho_C_sisters_B =   df_posterior[i,:rho_C_sisters_B],
        C =   df_posterior[i,:C],
      	H =  df_posterior[i,:H],
        rho_C_cousins =  df_posterior[i,:rho_C_cousins])

    for k in label_parameters
      df[i,k] = theta[k]
    end


    for c in 1:length(configs)
      initial_cell = configs[c][1]
      observation_time = configs[c][2]
      n_wells_Inc = configs[c][3]
      n_wells_MG = configs[c][4]
      nb_wells = max(n_wells_Inc, n_wells_MG)

      ### Simulate ###
      plate_c = grow_plate(
           initial_cell,
		       observation_time,
		       nb_wells, 
		       theta, 
		       empirical_distribution_T1, 
		       nb_cell_types = nb_cell_types, 
		       nb_gen_max = nb_gen_max,
		       shape_MG = :matrix,
		       noise = true, 
           ) #See ../dynamic_model/model_plate.jl

      ### Compute summary statistics ###
      #(see ../stat_model/summary stats.jl)

      ss_inc = compute_ss_incucyte(plate_c[2],
				nb_wells_used_for_computation = n_wells_Inc) 

      ss_MG = compute_ss_MG(plate_c[1],
			 nb_wells_used_for_computation = n_wells_MG)

      ss_col_size = compute_ss_colony_size(plate_c[3],
					nb_wells_used_for_computation = n_wells_Inc)

      #Store the results in the dataframe
      for k in keys(ss_inc)
        v = ss_inc[k]
        df[i,Symbol(string(k,"_start",map_cell_types[initial_cell]))] = v
      end

      for k in keys(ss_col_size)
        v = ss_col_size[k]
        df[i,Symbol(string(k,"_start",map_cell_types[initial_cell]))] = v
      end

      for k in keys(ss_MG)
        v = ss_MG[k]
        if typeof(v) == Float64
          df[i,Symbol(string(k,"_start",map_cell_types[initial_cell], 
			      "_obs", observation_time))] = v
        else
	        for iter in 1:length(v)
	          df[i,Symbol(string(k,
			         "_",iter,"_start",map_cell_types[initial_cell], 
			          "_obs", observation_time))] = v[iter]
       	  end
        end
      end

    end #Repeat for all configurations: initial cell x observation time

  end #Repeat over N simulations

  return df
end

