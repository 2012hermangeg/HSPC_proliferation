# HSPC Proliferation and Differentiation


## Introduction

In this project, we implement a model of Hematopoietic Stem and Progenitor Cell (HSPC) dynamics, focusing on the short-term proliferation and differentiation process.
The model is calibrated using experimental observation and ABC (Approximated Bayesian Computation) method. 

## Data

In this folder, we give the data required to run the code and the inference procedure.

## Dynamic Model

In this folder, we implement the proliferation and differentiation process.
We model the HSPC dynamics at the scale of a well (that it, we begin initially with a single cell of a given phenotype).
Simulationg the model consists of running the code at the scale of a plate (which consists of several wells).

## Stat model

The whole process can only be partially observed. Therefore, we need to define a observational model.
Since we estimate the model parameters within a Bayesian framework, we have to define a prior.
Since we are using an approximated Bayesian Computation method, we rely on the construction of a distance to the observations, and the definition of summary statistics.

## Simulate

In this folder, we simulate the model, sampling from the prior.
Creating a reference table is usefull to run the PCA method (and reduce the number of effective summary statistics to consider) and also the ABC-rejection method.

## ABC-SMC

In this folder, we implement our ABC-SMC sampling method.

## Post-treatment

In this folder, we post-treat the results and generate the figures.i
