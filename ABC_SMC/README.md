# ABC-SMC Module: ABC_SMC.jl

## Introduction

`ABC_SMC.jl` extends the capabilities of the earlier `simulate.jl` script (in ../simulate/  by implementing an Approximate Bayesian Computation Sequential Monte Carlo (ABC-SMC) method to refine parameter estimation based on observed data. This script is specifically tailored to operate under four main predefined scenarios, enhancing its precision in parameter inference by focusing on:
- Homogeneity in fate (H=1 or H=0)
- Concordance in division (C=1 or C=0)
- No concordance between cousin cells

## Key Features

- **Targeted Scenario Analysis**: Focused on four main scenarios to streamline computations and increase the relevance of the results to specific biological hypotheses.
- **Sequential Sampling and Weighting**: Employs a sophisticated sampling technique that evolves over iterations, adjusting parameter weights based on their likelihood given the observed data.
- **Parameter Space Exploration**: Utilizes a dynamic kernel to explore the parameter space efficiently, allowing the ABC-SMC algorithm to converge on the most probable parameter sets.

## Configuration and Usage

### Specific Configurations for ABC-SMC
- **Particle Count**: Configured to save a default of 1000 particles, each representing a vector of model parameters.
- **Iterative Process**: The number of iterations and the breadth of simulations per particle adjust dynamically to refine the parameter estimates progressively.
- **Tolerance Levels**: Employs a decreasing sequence of tolerance levels to gradually tighten the criteria for parameter acceptance, enhancing the accuracy of the inferred parameters.

### Dependencies and Includes
The script uses multiple Julia packages and local modules to perform its computations:
```julia
using Plots, Random, Distributions, StatsBase, CSV, DataFrames, UUIDs, LinearAlgebra
include various local model and statistical scripts
```

### Running ABC-SMC
The script is designed to be executed after setting up the appropriate configurations for the number of particles, number of iterations, tolerance levels, choice of the kernel, and other operational parameters.

## Example of Execution
To run `ABC_SMC.jl`, ensure that all related data files and scripts are correctly positioned as per the project's directory structure. Modify the script’s parameters as needed to align with specific experimental setups or analytical objectives.

## Output
- **Data Storage**: Results are stored in a designated directory (`results/` by default), with detailed records of each simulation step, including parameter values and their respective weights.
- **Final Results**: The script outputs a series of files that collectively represent the refined parameter space explored during the ABC-SMC process, providing a robust basis for subsequent analyses or model validations.


# Perturbation Kernels Module: kernel.jl

## Introduction

`kernel.jl` provides crucial functionality for the ABC-SMC (Approximate Bayesian Computation - Sequential Monte Carlo) process implemented in the `ABC_SMC.jl` script. This module defines perturbation kernels that are essential for generating new parameter vectors from existing ones, ensuring thorough exploration and efficient sampling of the parameter space under constrained scenario conditions.

## Key Features

- **Kernel Sampling**: Implements a perturbation kernel to sample new parameter vectors starting from an initial parameter set (`theta_ini`), adhering strictly to the bounds defined by the kernel's settings.
- **Scenario-specific Adjustments**: The kernels are tailored specifically for the four main scenarios considered in the ABC-SMC process, focusing on homogeneity in fate, concordance in division, and the absence of concordance between cousin cells.
- **Dynamic Adjustments**: Kernel parameters such as epsilon values and probability of staying in the current scenario (`p_stay`) are dynamically adjusted to fine-tune the sampling process as the ABC-SMC iterations progress.

## Functionality

### `kernel_sample`

- **Purpose**: Generates a new parameter vector by perturbing each element of the initial parameter vector within a defined range (`epsilon` values).
- **Parameters**: Receives `theta_ini`, an array representing the initial parameter vector.
- **Process**:
  - Each parameter is independently adjusted within a specific range determined by `epsilon_p` for the parameters associated with the transition probabilities, and `epsilon_mu` and `epsilon_sig` for parameters associated with the division times
   - Special handling is included to ensure that new parameters like probabilities remain valid (e.g., sum of certain probabilities does not exceed 1).

### `evaluate_kernel`

- **Purpose**: Evaluates how likely it is for a transition from one parameter vector (`theta_ini`) to another (`theta`) under the current kernel, which assists in weighting samples during the ABC-SMC process.
- **Parameters**: Takes two parameter vectors, `theta` and `theta_ini`, and checks if `theta` is within the support of the perturbation kernel centered at `theta_ini`.
- **Process**:
  - Checks if each parameter in `theta` falls within the allowable range around its corresponding value in `theta_ini`.
  - Computes a combined likelihood of staying in the same scenario or transitioning to a different scenario based on `p_stay`.

## Configuration and Usage

- **Kernel Parameters**: Defined dynamically in `ABC_SMC.jl` and tailored for specific iteration requirements. These parameters control the range and flexibility of parameter perturbations, crucial for exploring parameter space effectively.
- **Integration**: `kernel.jl` is included and used in `ABC_SMC.jl` to facilitate parameter sampling and adjustments across multiple iterations of the ABC-SMC process.


