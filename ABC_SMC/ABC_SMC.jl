############################
########  ABC - SMC ########
############################

#The content of this script has many similarities with
# ../simulate/simulate.jl
# Indeed, this latter file is designed to generate a reference table
# and then to perform a ABC-rejection method.
#
# Here, we directly perfom an ABC-SMC method
# Considering only the four main scenarios:
# - either Homogeneity in fate (H=1) or not (H=0)
# - either Concordance in division (C=1) or not (C=0)
# - No concordance between cousin cells

# Includes and cie
using Plots, Random, Distributions, StatsBase
using CSV, DataFrames
using UUIDs
using LinearAlgebra

include("../data/visualisation_tools.jl")
include("../dynamic_model/dividing.jl")
include("../dynamic_model/model_plate.jl")
include("../dynamic_model/model_well.jl")
include("../stat_model/observation_model.jl")
include("../stat_model/prior.jl")
include("../stat_model/summary_stats.jl")
include("../dynamic_model/support_functions.jl");
include("kernel.jl")


#########################################
### Configuration specific to ABC-SMC ###
#########################################

# Number of particles to save
N = 2000 #In ABC-SMC, a particle corresponds to a parameter vector

N_iter = 17
begin_iter = 0 #0 -> begin by sampling from the prior

#Nombre of time we simulate per particle, at iteration i
B = [1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 5]

#Decreasing sequence of tolerances
tolerance = [250, 200, 150, 100, 80, 70, 60, 50, 40, 30, 25, 23, 20, 18, 16, 15, 14]

# The uniform kernel (see kernel.jl) vary over the iterations
arr_epsilon_p = 0.08 .* ones(N_iter)
arr_epsilon_p[1:10] = [0.1 + 0.3^i for i in 1:10]
arr_epsilon_p[11:15] = [0.08 + 0.02^i for i in 1:5]

arr_epsilon_mu = 0.2  .* ones(N_iter)
arr_epsilon_mu[1:10] = [0.4 + 0.5^i for i in 1:10]
arr_epsilon_mu[11:15] = [0.2 + (0.4-0.2)^i for i in 1:5]

arr_epsilon_sig = 0.1  .* ones(N_iter)
arr_epsilon_sig[1:10] = [0.1 + 0.6^i for i in 1:10]

arr_eps_rec = 0.1 .* ones(N_iter)
arr_eps_rec[1:10] = [0.1 + 0.6^i for i in 1:10]

arr_p_stay_sc = 0.7 .* ones(N_iter)

##### PCA #####
# For the ABC-SMC method, we will directly compute the distance for each particle
# This requires compute the initial summary statistics for each simulation
# Then, project over the reduced space

# PCA projection matrix, with pc principal components
pc = 20
df_mat_proj = CSV.read(string("../simulate/pca/pca_proj",pc,"c.csv"), DataFrame)
mat_proj = Matrix(df_mat_proj)
#Information concerning the normalisation
mean_std = CSV.read("../simulate/pca/mean_std.csv", DataFrame)
mean_ss = mean_std[:,:m]
std_ss = mean_std[:,:std];
# Summary statistics computed from the real experimental data
ss_real = Vector(CSV.read("../data/ss_real.csv", DataFrame)[1,:])
#We normalize
ss_real_red = (ss_real.-mean_ss)./std_ss
# We reduce the dimension, to get the 20 first pca components
pca_real = (mat_proj'*ss_real_red)'



# Saving directory
rep_save = string("results/")


#################################
##### Some general settings #####
# Here, the same as in ../simulate/simulate.jl

nb_cell_types = 4;
nb_gen_max = 7;

# Experimental configurations 
# Array of quadruplet:
# (initial cell type, obs time, nb of wells for Inc, nb of wells for MG)
configs = [
	  (1, 72, 0, 135) 
	  (2, 72, 0, 167) 
	  (1, 96, 152, 271) #From HSC
	  (2, 96, 361, 261) #From MPP
	  (3, 96, 146, 247) #From HPC
	  ]
map_cell_types = ["HSC", "MPP", "HPC"]

### First division time ###
#T incucyte - first div - cocktail Diff - Bone Marrow
T_inc = CSV.read("../data/T1.csv", DataFrame) 

T1_HSC = T_inc[T_inc[:,:initial].=="HSC",:T1]
T1_MPP = T_inc[T_inc[:,:initial].=="MPP",:T1]
T1_HPC = T_inc[T_inc[:,:initial].=="HPC",:T1]

empirical_distribution_T1 = [T1_HSC, T1_MPP, T1_HPC]

### Define the prior distribution ###
prior = PriorDistribution("main") #Only choice possible (for now) 

if begin_iter == 0
  println("Begin by sampling from the prior")
  include("../simulate/simulate_from_prior.jl")
  df,  = simulate_from_prior(N, choice_prior = "main")
  
  df = df[:,1:24]
  df[:, :dist_L2] .= Inf  
  df[:, :weights] .= 1 ./ N

  CSV.write(string(rep_save, "step_0.csv"),df)

  begin_iter = 1
end

#########################
#### Run the ABC-SMC ####
#########################
println("Begin the ABc_SMC sampler with $N particles and $N_iter iterations")

for current_iteration in begin_iter:N_iter
  println()
  println(string("### iteration ",current_iteration," ###"))

  #Saving the dataframe each xth steps. If 0, no intermediate saves
  intermediate_saving = 100
  intermediate_counter = 0

  # Load previous_results
  previous_selected = CSV.read(string(rep_save, "step_",current_iteration-1,".csv"), 
                              DataFrame)

  N_particles = size(previous_selected, 1) 

  @show previous_distance = quantile(previous_selected[:,:dist_L2], 0.5)

  ### Prepare to store the results ###
  # Initialize a DataFrame
  df = DataFrame()

  # First colums for parameters
  label_parameters = get_labels_prior(prior)

  for label in label_parameters
    df[!, label] = zeros(Float64, N)
  end

  exp_MG = []
  exp_Inc = []
  for i in 1:length(configs)
    if configs[i][3] > 0 #obs for Inc
      push!(exp_Inc, configs[i][1])
    end
    if configs[i][4] > 0 #obs for MG
      push!(exp_MG, (configs[i][1],configs[i][2]))
    end
  end

  # Then, the colums associated to the summary statistics
  label_ss = get_labels_sum_stats(
		    exp_MG, #Diff config MG
		    exp_Inc, #Diff configs Inc
		    nb_gen_max = nb_gen_max,
		    nb_cell_types = nb_cell_types,
		    map_cell_types = map_cell_types
				)

  for label in label_ss
    df[!, label] = zeros(Float64, N)
  end

  df[!, :dist_L2] = zeros(Float64, N)
  df[!, :weights] = zeros(Float64, N)

  ##################
  ### Simulation ###
  ##################

  i = 1
  @time while i < N+1
   
    index_particle = sample(collect(1:N_particles), 
                        Weights(previous_selected[:,:weights]))

    sampled_particle = Vector(previous_selected[index_particle,1:end-2])
    moved_particle = kernel_sample(sampled_particle)
  
    v_prior = pdf(prior, moved_particle)

    if v_prior == 0.0
      continue
    end

    theta = (p1_1 =  moved_particle[1], 
        p1_2 =  moved_particle[2], 
        p1_3 =  moved_particle[3],
        mu_1 =  moved_particle[4], 
        sig_1 =  moved_particle[5],
        rho_H_1 =  moved_particle[6],
        p2_2 =  moved_particle[7], 
        p2_3 =  moved_particle[8], 
        mu_2 =  moved_particle[9], 
        sig_2 =  moved_particle[10],
        rho_H_2 =  moved_particle[11],
        p3_3 =  moved_particle[12],
        mu_3 =  moved_particle[13], 
        sig_3 =  moved_particle[14],
        rho_H_3 =  moved_particle[15],
        mu_4 =  moved_particle[16], 
        sig_4 =  moved_particle[17],
        recovery_rate =  moved_particle[18],
        rho_H_4 =  moved_particle[19],
        rho_C_sisters_A =  moved_particle[20],
        rho_C_sisters_B =  moved_particle[21],
        C =  moved_particle[22],
      	H =  moved_particle[23],
        rho_C_cousins =  moved_particle[24
        ])


    for k in label_parameters
      df[i,k] = theta[k]
    end

    nb_times_OK = 0
    mean_dist = 0.0
    for b in 1:B[current_iteration]

      #Loop over all configs
      for c in 1:length(configs)
        initial_cell = configs[c][1]
        observation_time = configs[c][2]
        n_wells_Inc = configs[c][3]
        n_wells_MG = configs[c][4]
        nb_wells = max(n_wells_Inc, n_wells_MG)

        #Simulate
        plate_c = grow_plate(initial_cell,
		       observation_time,
		       nb_wells, 
		       theta, 
		       empirical_distribution_T1, 
		       nb_cell_types = nb_cell_types, 
		       nb_gen_max = nb_gen_max,
		       shape_MG = :matrix,
		       noise = true, 
           )

        #Compute summary statistics
        ss_inc = compute_ss_incucyte(plate_c[2],
			          nb_wells_used_for_computation = n_wells_Inc) 

        ss_MG = compute_ss_MG(plate_c[1],
			        nb_wells_used_for_computation = n_wells_MG)

        ss_col_size = compute_ss_colony_size(plate_c[3],
					    nb_wells_used_for_computation = n_wells_Inc)

        #Store the results in the dataframe
        for k in keys(ss_inc)
          v = ss_inc[k]
          df[i,Symbol(string(k,"_start",map_cell_types[initial_cell]))] = v
        end

        for k in keys(ss_col_size)
          v = ss_col_size[k]
          df[i,Symbol(string(k,"_start",map_cell_types[initial_cell]))] = v
        end

        for k in keys(ss_MG)
          v = ss_MG[k]
          if typeof(v) == Float64
            df[i,Symbol(string(k,"_start", map_cell_types[initial_cell], 
			            "_obs", observation_time))] = v
          else
      	    for iter in 1:length(v)
	            df[i,Symbol(string(k,
			            "_",iter,"_start",map_cell_types[initial_cell], 
			            "_obs", observation_time))] = v[iter]
       	    end
          end
        end

      end #Repeat for all configurations: initial cell x observation time
      
      vect_ss = Vector(df[i,25:end-2])
      vect_ss_red = (vect_ss.-mean_ss)./std_ss
  
      pca_sim = (mat_proj'*vect_ss_red)'
      dist_b = sum((pca_sim .- pca_real).^2)
      if dist_b < tolerance[current_iteration]
        nb_times_OK += 1
      end
      mean_dist += dist_b

    end
    mean_dist /= B[current_iteration]

    df[i, :dist_L2] = mean_dist

    if nb_times_OK == 0
      continue
    else
      #We keep the particle
      #Then, we compute its new weight
      #(Which should be renormalized later)
      den_w = 0
      for j in 1:size(previous_selected,1)
      
        den_w += previous_selected[j,:weights]*evaluate_kernel(
                                    moved_particle, 
                                    Vector(previous_selected[j,1:end-2])) 
      end
      df[i, :weights] = nb_times_OK*v_prior/den_w
      i += 1
    end

    intermediate_counter +=1
    if intermediate_counter == intermediate_saving
      intermediate_counter = 0
      CSV.write(string(rep_save, "step_",current_iteration,".csv"),
                    df[:,[1:24;size(df, 2)-1:size(df, 2)]])
    
    end

  end #Repeat over N simulations (until having N particle)

  sum_weights = sum(df[:,:weights])
  #We normalize the weights
  df[:,:weights] = df[:,:weights]./sum_weights

  println("save iteration")
  CSV.write(string(rep_save, "step_",current_iteration,".csv"), 
            df[:,[1:24;size(df, 2)-1:size(df, 2)]])

  println("###")
  println()

end
