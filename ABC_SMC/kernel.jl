############################
### Perturbation kernels ###
### used for the ABC-SMC ###
############################

#NB: only for the 4 main scenarios

function kernel_sample(theta_ini)
  """
  Implementation of the perturbation kernel
  start from theta_ini to sample a new parameter vector
  """

  (p1_1_ini, 
       p1_2_ini, 
       p1_3_ini,
       mu_1_ini, 
       sig_1_ini,
       rho_H_1_,
       p2_2_ini, 
       p2_3_ini, 
       mu_2_ini, 
       sig_2_ini,
       rho_H_2_,
       p3_3_ini,
       mu_3_ini, 
       sig_3_ini,
       rho_H_3_ini,
       mu_4_ini, 
       sig_4_ini,
       recovery_rate_ini,
       rho_H_4_ini,
       rho_C_sisters_A_ini,
       rho_C_sisters_B_ini,
       C_ini,
       H_ini,
       rho_C_cousins_ini
	) = theta_ini
 
  #The following arrays are defined in the script ABC_SMC
  #(the one which call kernel.jl)
  epsilon_p = arr_epsilon_p[N_iter]
  epsilon_mu = arr_epsilon_mu[N_iter]
  epsilon_sig = arr_epsilon_sig[N_iter]
  eps_rec = arr_eps_rec[N_iter]
  p_stay = arr_p_stay_sc[N_iter]
  ###
  
  p1_1 = rand(Uniform(p1_1_ini - epsilon_p, p1_1_ini + epsilon_p))
  p1_2 = rand(Uniform(p1_2_ini - epsilon_p, p1_2_ini + epsilon_p))
  p1_3 = rand(Uniform(p1_3_ini - epsilon_p, p1_3_ini + epsilon_p))
  while p1_1 + p1_2 + p1_3 > 1 
    p1_1 = rand(Uniform(p1_1_ini - epsilon_p, p1_1_ini + epsilon_p))
    p1_2 = rand(Uniform(p1_2_ini - epsilon_p, p1_2_ini + epsilon_p))
    p1_3 = rand(Uniform(p1_3_ini - epsilon_p, p1_3_ini + epsilon_p))
  end 

  p2_2 = rand(Uniform(p2_2_ini - epsilon_p, p2_2_ini + epsilon_p))
  p2_3 = rand(Uniform(p2_3_ini - epsilon_p, p2_3_ini + epsilon_p))
  while p2_2 + p2_3 > 1 
    p2_2 = rand(Uniform(p2_2_ini - epsilon_p, p2_2_ini + epsilon_p))
    p2_3 = rand(Uniform(p2_3_ini - epsilon_p, p2_3_ini + epsilon_p))
  end

  p3_3 = rand(Uniform(p3_3_ini - epsilon_p, p3_3_ini + epsilon_p))
  
  mu_1 = rand(Uniform(mu_1_ini - epsilon_mu, mu_1_ini + epsilon_mu))
  mu_2 = rand(Uniform(mu_2_ini - epsilon_mu, mu_2_ini + epsilon_mu))
  mu_3 = rand(Uniform(mu_3_ini - epsilon_mu, mu_3_ini + epsilon_mu))
  mu_4 = rand(Uniform(mu_4_ini - epsilon_mu, mu_4_ini + epsilon_mu))

  sig_1 = rand(Uniform(sig_1_ini - epsilon_sig, sig_1_ini + epsilon_sig))
  sig_2 = rand(Uniform(sig_2_ini - epsilon_sig, sig_2_ini + epsilon_sig))
  sig_3 = rand(Uniform(sig_3_ini - epsilon_sig, sig_3_ini + epsilon_sig))
  sig_4 = rand(Uniform(sig_4_ini - epsilon_sig, sig_4_ini + epsilon_sig))

  recovery_rate = rand(Uniform(recovery_rate_ini - eps_rec,recovery_rate_ini + eps_rec))

  C = C_ini
  H = H_ini
 
  #First, deal with the scenario of homogeneity
  rho_H_1 = 0.0
  rho_H_2 = 0.0
  rho_H_3 = 0.0
  rho_H_4 = 0.0

  if H_ini == 1
    #Same homogeneity between all sister cells
    if rand() < p_stay #Probablity to stay in the "scenario"
      H == 1
      #But we move the associated parameters
      rho_H_1 = rand(Uniform(0.3, 1.0))
      rho_H_2 = rho_H_1
      rho_H_3 = rho_H_1
     else
       H = 0
     end

  elseif H_ini == 0
    if rand() < p_stay
      H = 0 #We stay
    else
      H = 1
      rho_H_1 = rand(Uniform(0.3, 1.0))
      rho_H_2 = rho_H_1
      rho_H_3 = rho_H_1
    end
  end

  #Then, deal with the concordance in division
   rho_C_sisters_A = rho_C_sisters_A_ini
   if C_ini == 1
     if rand() < p_stay
       C = 1
       rho_C_sisters_A = rand(Uniform(0.3,1))
      else
        C = 0
        rho_C_sisters_A = 0.0
      end
    elseif C_ini == 0
      if rand() < p_stay
        C = 0
        rho_C_sisters_A = 0.0
      else
        C = 1
        rho_C_sisters_A  = rand(Uniform(0.3,1))
      end
    end

  rho_C_sisters_B = 0.0 #Not used when considering only one the the 4 main scenarios

  #Recall that there is no concordance between sister for the 4 main scenarios
  rho_C_cousins = 0.0

  return [p1_1, 
        p1_2, 
        p1_3,
        mu_1, 
        sig_1,
        rho_H_1,
        p2_2, 
        p2_3, 
        mu_2, 
        sig_2,
        rho_H_2,
        p3_3,
        mu_3, 
        sig_3,
        rho_H_3,
        mu_4, 
        sig_4,
        recovery_rate,
        rho_H_4,
        rho_C_sisters_A,
        rho_C_sisters_B,
        C,
      	H,
        rho_C_cousins
      	]
end

function evaluate_kernel(theta, theta_ini)
  """
  Evaluate the pertuarbation kernel,
  in theta
  starting from theta_ini
  """

  #Basically, since we consider a uniform perturbation kernek
  #We especiallu wonder whether theta in the support of the kernel from theta_ini

   (p1_1, 
        p1_2, 
        p1_3,
        mu_1, 
        sig_1,
        rho_H_1,
        p2_2, 
        p2_3, 
        mu_2, 
        sig_2,
        rho_H_2,
        p3_3,
        mu_3, 
        sig_3,
        rho_H_3,
        mu_4, 
        sig_4,
        recovery_rate,
        rho_H_4,
        rho_C_sisters_A,
        rho_C_sisters_B,
        C,
      	H,
        rho_C_cousins
	) = theta


  (p1_1_ini, 
        p1_2_ini, 
        p1_3_ini,
        mu_1_ini, 
        sig_1_ini,
        rho_H_1_,
        p2_2_ini, 
        p2_3_ini, 
        mu_2_ini, 
        sig_2_ini,
        rho_H_2_,
        p3_3_ini,
        mu_3_ini, 
        sig_3_ini,
        rho_H_3_ini,
        mu_4_ini, 
        sig_4_ini,
        recovery_rate_ini,
        rho_H_4_ini,
        rho_C_sisters_A_ini,
        rho_C_sisters_B_ini,
        C_ini,
      	H_ini,
        rho_C_cousins_ini
	) = theta_ini

  epsilon_p = arr_epsilon_p[N_iter]
  epsilon_mu = arr_epsilon_mu[N_iter]
  epsilon_sig = arr_epsilon_sig[N_iter]
  eps_rec = arr_eps_rec[N_iter]
  p_stay = arr_p_stay_sc[N_iter]

  #mu
  if mu_1 < mu_1_ini - epsilon_mu || mu_1 > mu_1_ini + epsilon_mu
    return 0
  end

  if mu_2 < mu_2_ini - epsilon_mu || mu_2 > mu_2_ini + epsilon_mu
    return 0
  end

  if mu_3 < mu_3_ini - epsilon_mu || mu_3 > mu_3_ini + epsilon_mu
    return 0
  end

  if mu_4 < mu_4_ini - epsilon_mu || mu_4 > mu_4_ini + epsilon_mu
    return 0
  end


  #sig
  if sig_1 < sig_1_ini - epsilon_sig || sig_1 > sig_1_ini + epsilon_sig
    return 0
  end

  if sig_2 < sig_2_ini - epsilon_sig || sig_2 > sig_2_ini + epsilon_sig
    return 0
  end

  if sig_3 < sig_3_ini - epsilon_sig || sig_3 > sig_3_ini + epsilon_sig
    return 0
  end

  if sig_4 < sig_4_ini - epsilon_sig || sig_4 > sig_4_ini + epsilon_sig
    return 0
  end


  if recovery_rate < recovery_rate_ini - eps_rec || recovery_rate > recovery_rate_ini + eps_rec
    return 0  
  end


      
  #pi_j


  if p1_1 < p1_1_ini - epsilon_p || p1_1 > p1_1_ini + epsilon_p
    return 0
  end
  if p1_2 < p1_2_ini - epsilon_p || p1_2 > p1_2_ini + epsilon_p
    return 0
  end
  if p1_3 < p1_3_ini - epsilon_p || p1_3 > p1_3_ini + epsilon_p
    return 0
  end

  if p2_2 < p2_2_ini - epsilon_p || p2_2 > p2_2_ini + epsilon_p
    return 0
  end
  if p2_3 < p2_3_ini - epsilon_p || p2_3 > p2_3_ini + epsilon_p
    return 0
  end

  if p3_3 < p3_3_ini - epsilon_p || p3_3 > p3_3_ini + epsilon_p
    return 0
  end


  #Now, consider the potential change of scenarios
  eval_kernel = 1.0

  if C_ini == 1
    if C == 1
      eval_kernel *= p_stay/0.7
    else 
       eval_kernel *= (1-p_stay)
    end
  elseif C_ini == 0
    if C == 0
      eval_kernel *= p_stay
    else
      eval_kernel *= (1-p_stay)/0.7
    end
  end

  if H_ini == 1
    if H == 1
      eval_kernel *= p_stay/0.7
    else 
       eval_kernel *= (1-p_stay)
    end
  elseif H_ini == 0
    if H == 0
      eval_kernel *= p_stay
    else
      eval_kernel *= (1-p_stay)/0.7
    end
  end

  return eval_kernel

end


