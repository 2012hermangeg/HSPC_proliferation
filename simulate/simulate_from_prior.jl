###############################################
### Simulate from the prior
### and compute summary statistics 
###############################################


# Includes and cie
using Plots, Random, Distributions, StatsBase
using CSV, DataFrames
using UUIDs

include("../data/visualisation_tools.jl")
include("../dynamic_model/dividing.jl")
include("../dynamic_model/model_plate.jl")
include("../dynamic_model/model_well.jl")
include("../stat_model/observation_model.jl")
include("../stat_model/prior.jl")
include("../stat_model/summary_stats.jl")
include("../dynamic_model/support_functions.jl");


function simulate_from_prior(N; rep_save = "", 
                                choice_prior = "main",
                                sc_H = 0,
                                sc_C = 0,
                                intermediate_saving = 0, 

                                )
  """
  simulate and return a dataframe
  with N lines
  For each line, we have a parameter vector sampled from the prior 'choice_prior'
  and the associated computed summary statistics
  
  option:
  intermediate_saving: Saving the dataframe each xth steps. If 0, no intermediate saves

  if choice_prior == "solo", then sc_H and sc_C have to be defined
  """

  intermediate_counter = 0

  ### Some general settings ###
  nb_cell_types = 4;
  nb_gen_max = 7;

  # Experimental configurations 
  # Array of quadruplet:
  # (initial cell type, obs time, nb of wells for Inc, nb of wells for MG)
  configs = [
	  (1, 72, 0, 135) 
	  (2, 72, 0, 167) 
	  (1, 96, 152, 271) #From HSC
	  (2, 96, 361, 261) #From MPP
	  (3, 96, 146, 247) #From HPC
	  ]
  map_cell_types = ["HSC", "MPP", "HPC"]
 
  ### First division time ###
  #T incucyte - first div - cocktail Diff - Bone Marrow
  T_inc = CSV.read("../data/T1.csv", DataFrame) 

  T1_HSC = T_inc[T_inc[:,:initial].=="HSC",:T1]
  T1_MPP = T_inc[T_inc[:,:initial].=="MPP",:T1]
  T1_HPC = T_inc[T_inc[:,:initial].=="HPC",:T1]

  empirical_distribution_T1 = [T1_HSC, T1_MPP, T1_HPC]


  ### Define the prior distribution ###
  prior = choice_prior == "solo" ?
            PriorDistribution(choice_prior, sc_H, sc_C) : 
            PriorDistribution(choice_prior) #"main" or "all"

  ### Prepare to store the results ###
  # Initialize a DataFrame
  df = DataFrame()

  # First colums for parameters
  label_parameters = get_labels_prior(prior)

  for label in label_parameters
    df[!, label] = zeros(Float64, N)
  end

  exp_MG = [] #MG - MultiGen
  exp_Inc = [] # Inc - Incucyte (Live Cell Imaging)
  for i in 1:length(configs)
    if configs[i][3] > 0 #obs for Inc
      push!(exp_Inc, configs[i][1])
    end
    if configs[i][4] > 0 #obs for MG
      push!(exp_MG, (configs[i][1],configs[i][2]))
    end
  end

  # Then, the colums associated to the summary statistics
  label_ss = get_labels_sum_stats(
		    exp_MG, #Diff config MG
		    exp_Inc, #Diff configs Inc
		    nb_gen_max = nb_gen_max,
		    nb_cell_types = nb_cell_types,
		    map_cell_types = map_cell_types
				)

  for label in label_ss
    df[!, label] = zeros(Float64, N)
  end

  @show id_sim = uuid4() #Will be usefull when saving the results

  ##################
  ### Simulation ###
  ##################

  println()
  println("### Begin computation ###")
  println(string("with N = ",N))
  println()

  @time for i in 1:N
  
    theta = rand(prior)
    for k in label_parameters
      df[i,k] = theta[k]
    end


    for c in 1:length(configs)
      initial_cell = configs[c][1]
      observation_time = configs[c][2]
      n_wells_Inc = configs[c][3]
      n_wells_MG = configs[c][4]
      nb_wells = max(n_wells_Inc, n_wells_MG)

      ### Simulate ###
      plate_c = grow_plate(
           initial_cell,
		       observation_time,
		       nb_wells, 
		       theta, 
		       empirical_distribution_T1, 
		       nb_cell_types = nb_cell_types, 
		       nb_gen_max = nb_gen_max,
		       shape_MG = :matrix,
		       noise = true, 
           ) #See ../dynamic_model/model_plate.jl

      ### Compute summary statistics ###
      #(see ../stat_model/summary stats.jl)

      ss_inc = compute_ss_incucyte(plate_c[2],
				nb_wells_used_for_computation = n_wells_Inc) 

      ss_MG = compute_ss_MG(plate_c[1],
			 nb_wells_used_for_computation = n_wells_MG)

      ss_col_size = compute_ss_colony_size(plate_c[3],
					nb_wells_used_for_computation = n_wells_Inc)

      #Store the results in the dataframe
      for k in keys(ss_inc)
        v = ss_inc[k]
        df[i,Symbol(string(k,"_start",map_cell_types[initial_cell]))] = v
      end

      for k in keys(ss_col_size)
        v = ss_col_size[k]
        df[i,Symbol(string(k,"_start",map_cell_types[initial_cell]))] = v
      end

      for k in keys(ss_MG)
        v = ss_MG[k]
        if typeof(v) == Float64
          df[i,Symbol(string(k,"_start",map_cell_types[initial_cell], 
			      "_obs", observation_time))] = v
        else
	        for iter in 1:length(v)
	          df[i,Symbol(string(k,
			         "_",iter,"_start",map_cell_types[initial_cell], 
			          "_obs", observation_time))] = v[iter]
       	  end
        end
      end

    end #Repeat for all configurations: initial cell x observation time

    intermediate_counter +=1
    if intermediate_counter == intermediate_saving
      intermediate_counter = 0
      CSV.write(string(rep_save,"sim_",id_sim,".csv"), df)   
    end

  end #Repeat over N simulations

  return df, id_sim
end

