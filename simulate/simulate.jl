###############################################
### Simulate and compute summary statistics ###
###############################################

# Here, one simulation will consist of computing the dynamics
# for a high number of wells / families 
# The dynamics computed for a well / family will be called trajectory
# A simulation is a set of a high number of trajectories
# From a simulation, we compute and store summary statistics
# Which could ultimately be compared to the ones we get from real observations

include("simulate_from_prior.jl")

# Number of simulations
N = 10 

# Saving directory
rep_save = "simulations/"

intermediate_saving = 100

df, id_sim = simulate_from_prior(N, 
        rep_save=rep_save,
        intermediate_saving=intermediate_saving,
        choice_prior = "solo", sc_H = 1, sc_C = 1)


#Save the final results
CSV.write(string(rep_save,"sim_",id_sim,".csv"), df)

