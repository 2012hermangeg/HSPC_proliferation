###################
### Model Plate ###
###################

function grow_plate(initial_cell_type::Int64,
		        observation_time::Union{Float64,Int64},
		      	nb_wells::Int64,
	      		theta::NamedTuple,
	      		empirical_T1::Vector{Vector{Int64}};
	      		noise=false,
	      		nb_gen_max = 7,
		      	nb_cell_types = 4,
		      	shape_MG = :array_matrix
			)
  """
  Simulate the proliferation and differentiation dynamics
  of several wells.
  A plate is, here, defined as a set of several wells,
  each with the same initial cell type,
  each with the same observation time.
  Here, only the quantity of interests wil lbe observed for each well.
  The observation model (left-censor & sampling) might be applied

  # Required arguments:
  - `initial_cell_type::Int64`: Numerical value associated to the initial cell
  - `observation_time::Float64`: Time when we observe the content of the well [in hours]
  - `nb_wells::Int64`: Nb of times we simulate the proliferation of a cell in a well
  - `theta::NamedTuple`: Parameter vector
  - `empirical_T1::Vector{Vector{Int64}}`: Each ith element of this Array contains a list of the observed left-censored first division times of cell of type i. 

  # Optional arguments:
  - `shape_MG`: shape of the MultiGen output: :array_matrix or :matrix
  - `nb_gen_max`: highest observable generation (generation of the first cell is 1)
  - `noise`: if true, we apply a sampling noise (recovery) and a left-interval censor for the time of first T1 and ulterior divisions
  - `nb_cell_types`: nb of cell types, by default 4 (1-> HSC, 2-> MPP, 3->HPC, 4->CD34-)
  """

  #MultiGen
  mat_MG = shape_MG == :array_matrix ?
  			Array{Any}(undef, nb_wells) : 
		  	zeros(Int64, nb_wells, nb_gen_max*nb_cell_types)

  #Live Cell Imaging
  mat_Incucyte = zeros(Float64, nb_wells, 7) #We observe T1, T2_1 and T2_2, and 4 values of T3 through the Live Cell Imaging (Incucyte) assay
  vect_colony_size = zeros(Int64, nb_wells) #Number of cells at the end of the exp.

  #Construct elements required for the simulations
  transition_matrix = construct_transition(theta, nb_cell_types)
  homogeneity_vector = [theta[Symbol(string("rho_H_",i))] for i in 1:nb_cell_types]
  mu_div_vector = [theta[Symbol(string("mu_",i))] for i in 1:nb_cell_types]
  std_div_vector = [theta[Symbol(string("sig_",i))] for i in 1:nb_cell_types]
  
  for i in 1:nb_wells
    #Simulate the proliferation process within each well

    simu_well = grow(initial_cell_type, 
		     theta, 
		     observation_time,
		     empirical_T1,
		     save_state = false,
		     transition_matrix = transition_matrix,
		     homogeneity_vector = homogeneity_vector,
		     mu_div_vector = mu_div_vector,
		     std_div_vector = std_div_vector,
		     nb_gen_max = nb_gen_max,
		     nb_cell_types = nb_cell_types
		     ) #defined in model_well.jl

    # Output associated to the MultiGen assay
    if shape_MG == :array_matrix
      mat_MG[i] = deepcopy(simu_well[1])
    elseif shape_MG == :matrix
      #Reshape
      mat_MG[i,:] = reshape(simu_well[1],nb_gen_max*nb_cell_types) 
    else
      println("error: shape_MG should be either :matrix or :array_matrix")
    end

    # Output associated to the Live Cell Imaging Assay
    mat_Incucyte[i,:] = simu_well[2]
    vect_colony_size[i] = sum(simu_well[1])
  end


  if noise
    #we consider some noise (functions defined in ../stat_model/observation_model.jl)

    #First, we apply a sampling noise
    if shape_MG == :array_matrix
      for i in 1:nb_wells
        mat_MG[i] = sampling_noise_MG(mat_MG[i],recovery_rate=theta[:recovery_rate])
      end
    elseif shape_MG == :matrix
        mat_MG = sampling_noise_MG(mat_MG,recovery_rate=theta[:recovery_rate])
    end

    #Then, we apply a left-censorhip for the observation time
    #But we also sort the second division times
    #By default, an unobserved division has a time equal to Inf
    #If left censored, Inf becomes equal to the observation time
    mat_Incucyte = censor_Incucyte(mat_Incucyte, observation_time, censor=:left)
  end

  return (mat_MG, mat_Incucyte, vect_colony_size)

end

