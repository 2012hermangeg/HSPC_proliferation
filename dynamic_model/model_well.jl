##################################
### HSPC Proliferation - Model ###
##################################

function grow(initial_cell_type::Int64, 
		theta::NamedTuple,
		observation_time::Union{Float64,Int64},
		empirical_T1::Vector{Vector{Int64}};
		save_state = false,
		transition_matrix = nothing,
		homogeneity_vector = nothing,
		mu_div_vector = nothing,
		std_div_vector = nothing,
		nb_gen_max = 7,
		nb_cell_types = 4
	      )
  """
  Simulate the dynamics of proliferation and differentiation
  From a single-cell within a well

  # Required arguments:
  - `initial_cell_type::Int64`: Numerical value associated to the initial cell
  - `theta::NamedTuple`: Parameter vector
  - `observation_time::Float64`: Time when we observe the content of the well [in hours]
  - `empirical_T1::Vector{Vector{Int64}}`: Each ith element of this Array contains a list of the observed left-censored first division times of cell of type i. 
  
  # Optional arguments:
  - `save_state`: by default: false. If true, the whole system state will be saved and returned
  - `transition_matrix`: If not provided, will we computed from theta
  - `homogeneity_vector`: If not provided, will be computed from theta
  - `mu_div_vector`: If not provided, will be computed from theta
  - `std_div_vector`: If not provided, will be computed from theta
  - `nb_gen_max`: highest observable generation (generation of the first cell is 1)

  # Return if save state == false
  - (gen_type_matrix, (T1, T2_daughter1, T2_daughter2, T3_1, T3_2, T3_3, T3_4)) 
  where the order between T2_daughter1 and T2_daughter2 is random

  # Return if save state == true
  - (gen_type_matrix, [T1, T2_daughter1, T2_daughter2, T3_1, T3_2, T3_3, T3_4))], p, m, TA)
  where :
   - `p::Vector{Int64}`: corresponds to the cell type of each ith cell
   appeared in the system
  - `m::Vector{Int64}`: index of the mother of the ith cell
  - `TA::Vector{Float64}`: apparition time of the ith cell
  - `nb_cell_types`: nb of cell types, by default 4 (1-> HSC*, 2-> MPP, 3->HPC, 4->MPP)
  """

  ### Set-up ###
  if transition_matrix == nothing
    transition_matrix = construct_transition(theta, nb_cell_types)
  end
  if homogeneity_vector == nothing
    homogeneity_vector = [theta[Symbol(string("rho_H_",i))] for i in 1:nb_cell_types]
  end
  if mu_div_vector == nothing
    mu_div_vector = [theta[Symbol(string("mu_",i))] for i in 1:nb_cell_types]
  end
  if std_div_vector == nothing
    std_div_vector = [theta[Symbol(string("sig_",i))] for i in 1:nb_cell_types]
  end

  # Minimal state variables required to further compute the observation variables
  # at the end, gen_type_matrix[i,j] will give:
  # the number of cells, at the observation time,
  # being of type j
  # and in the generation i
  gen_type_matrix = zeros(Int64,nb_gen_max, nb_cell_types)
  gen_type_matrix[1,initial_cell_type] = 1 #Initialisation, one cell in first generation
  
  T1 = Inf
  T2_daughter1 = Inf
  T2_daughter2 = Inf

  T3 = [Inf, Inf, Inf, Inf]

  # Full state if save_state == true
  p = [initial_cell_type] #store the cell type 
  m = [0] #Store the index of the mother (by convention, equal to 0 for the initial cell)
  #NB: the index in the previous vector correspond to the cell index
  TA = [0.0] #Store the apparition time of the ith cell


  ### Proliferation and differentiation ###
  # Sample first division time
  # empirical_T1 corresponds to the empirical discrete distribution
  # associated with the observations (interval-censored, left).
  # To have a continous variable, we add a random value between 0 and 1
  
  T1 = rand(empirical_T1[initial_cell_type])+rand() 


  # If the divisions occurs before the observation time, continue
  if T1 <= observation_time
    ### Continue to grow ###

    # 2 daughters cells appeared at T1
    # We determine their cell types
    daughter1_type = sample(collect(1:nb_cell_types),
		                	Weights(transition_matrix[initial_cell_type,:]))
    daughter2_type = sample(collect(1:nb_cell_types), 
		                	Weights(transition_matrix[initial_cell_type,:]))

    # Account for potential homogeneity
    # That is, an additional probability that the 2 daughter cells have the same type
    # The higher the homogeneity, the higher this proba
    if rand() <= homogeneity_vector[initial_cell_type] 
      daughter2_type = daughter1_type
    end

    # Update the gen_type_matrix
    gen_type_matrix[1,initial_cell_type] -= 1 #Initial cell disappeared when dividing
    # 2 daugther cells appeared, at generation 2
    gen_type_matrix[2,daughter1_type] += 1 
    gen_type_matrix[2,daughter2_type] += 1
    
    if save_state
      # We keep track of each cell having existed
      push!(p, daughter1_type)
      push!(p, daughter2_type)
      push!(m, 1) #The first cell (index 1) is their mother
      push!(m, 1)
      push!(TA, T1)
      push!(TA, T1)
    end

    #First, we consider the two daughter cells and determine their division time
    cov_mat_sisters = construct_cov_mat([daughter1_type, daughter2_type, 1, 1],
					theta,
					nb_cell_types,
					std_div_vector=std_div_vector
					)[1:2,1:2]
    #NB: Here, we provided dummy informations about cousins since they do no exist yet
    
    (T2_daughter1, T2_daughter2) = rand(MvLogNormal(
				      [mu_div_vector[daughter1_type],mu_div_vector[daughter2_type]], 
			       	cov_mat_sisters)
              ) .+ T1

    if min(T2_daughter1, T2_daughter2) <= observation_time
      # If at least one daughter cell divides before the observation time,
      # we consider the 4 cousin cells, and make them differentiate and proliferate

      division_cousins((T2_daughter1, T2_daughter2),
			(daughter1_type, daughter2_type),
			2, #Generation
			theta,
			observation_time,
			gen_type_matrix,
			transition_matrix,
			homogeneity_vector,
			std_div_vector,
			mu_div_vector,
			save_state,
			p,
			m,
			TA,
			T3,
			(2,3), #Indexes of daughter1 and 2
			nb_cell_types,
			nb_gen_max
			)

    end

  end #End proliferation

  if save_state
    return (gen_type_matrix, [T1, T2_daughter1, T2_daughter2, T3[1], T3[2], T3[3], T3[4]], p, m, TA)
  else
    return (gen_type_matrix, [T1, T2_daughter1, T2_daughter2, T3[1], T3[2], T3[3], T3[4]])
  end

end








