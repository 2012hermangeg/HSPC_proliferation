# Dividing

## Introduction

This Julia function, `division_cousins`, simulates the recursive differentiation and proliferation of four cousin cells from two sister cells. It models cellular behavior over time, tracking cell types, division times, and generations.

## Functionality

- Simulates cell division, accounting for differentiation and proliferation across generations.
- Tracks cell lineage, including type and generation.
- Optionally saves cell state information for further analysis.
- Customizable through a range of parameters including observation time and cell type transition probabilities.

## Key Parameters

- **Division Times** (`T_sister1`, `T_sister2`): Times at which sister cells divide.
- **Cell Types** (`sister1_type`, `sister2_type`): Types of the sister cells.
- **Generation** (`gen`): Current generation, starting from 1.
- **Observation Time** (`observation_time`): Simulation end time, specified in hours.
- **Matrices and Vectors**: Various inputs like `gen_type_matrix`, `transition_matrix`, `homogeneity_vector`, etc., controlling the simulation dynamics.
- **Save State** (`save_state`): When `true`, additional details (cell types, parentage) are saved.

## Usage

Designed for studies on cellular differentiation, proliferation, and lineage tracking. The function requires specific inputs detailed in the code's documentation comment, including initial conditions, cell type matrices, and simulation parameters.



# Support Functions 

## Introduction

The `support_functions.jl` file contains essential  functions that support the main cell division simulation by constructing transition matrices and covariance matrices. These functions play a crucial role in modeling the probabilities of cell type transitions and the correlations in division times among cells.

## Functions Overview

### `construct_transition`

This function generates a transition matrix representing the probabilities that a daughter cell will transition into any given cell type based on the type of its mother cell. It enforces the rule that de-differentiation (transitioning to a "less differentiated" cell type) is not permitted.

- **Inputs:**
  - `theta`: A named tuple containing parameters for the probabilities.
  - `nb_cell_types`: The number of distinct cell types in the simulation.

- **Output:** 
  - A transition matrix where each element `M[i,j]` represents the probability that a cell of type `i` will produce a daughter cell of type `j`.

### `construct_cov_mat`

Constructs a 4x4 covariance matrix used in the multivariate log-normal distribution to sample division times of cousin cells under various concordance scenarios.

- **Inputs:**
  - `cell_types`: A vector specifying the types of the four cousin cells involved.
  - `theta`: A named tuple containing parameters influencing the covariance matrix.
  - `nb_cell_types`: The number of cell types considered in the model.
  - `std_div_vector` (optional): A vector specifying the standard deviations used in the covariance matrix. If not provided, it is computed from `theta`.

- **Output:** 
  - A covariance matrix that accounts for both the variability in division times and the concordance among sister and cousin cells.

## Usage

These support functions are integral to the cell division simulation, providing detailed control over the biological assumptions of the model, such as the prohibition of de-differentiation and the precise management of cell type transitions and division time correlations.

To use these functions, include `support_functions.jl` in your Julia project and call them with the required parameters as part of your simulation setup.



# Model well

## Introduction

`model_well.jl` defines a Julia function, `grow`, aimed at simulating the proliferation and differentiation dynamics of hematopoietic stem and progenitor cells (HSPCs) from a single cell within a well over a specified observation period.

## Key Features

- **Cell Type Dynamics**: Tracks the transitions of cell types based on empirical and model-driven probabilities.
- **Observation Time Frame**: Simulates the cell proliferation process up to a predefined observation time.
- **Empirical Division Times**: Incorporates empirically observed first division times to model cell proliferation behavior.
- **Optional State Saving**: Allows for the detailed tracking of the system state, including cell types, division times, and lineages.

## Parameters

- **`initial_cell_type`**: The type of the initial cell placed in the well.
- **`theta`**: A named tuple containing model parameters.
- **`observation_time`**: The period over which the cell proliferation is observed.
- **`empirical_T1`**: Observed left-censored first division times for cells of type i.

### Optional Arguments

- **`save_state`**: When set to `true`, the function saves and returns the entire system state.
- **`transition_matrix`**, **`homogeneity_vector`**, **`mu_div_vector`**, **`std_div_vector`**: Model parameters, auto-computed from `theta` if not provided.
- **`nb_gen_max`**: The maximum observable cell generation.
- **`nb_cell_types`**: The total number of different cell types in the simulation, defaulting to 4.

## Returns

- **Without state saving**: A tuple containing the generation type matrix and an array with division times (`T1`, `T2_daughter1`, `T2_daughter2`).
- **With state saving**: In addition to the above, returns vectors `p` (cell types), `m` (mother cell indices), and `TA` (cell apparition times).

## Usage

The function is utilized to model the growth of a cell population within a well, considering cell differentiation and proliferation patterns. It's particularly useful in stem cell research and simulations involving HSPCs.



# Model Plate

## Introduction

`model_plate.jl` introduces a Julia function, `grow_plate`, designed to simulate the dynamics of proliferation and differentiation across multiple wells in a plate. This simulation incorporates various parameters to model the growth patterns of cells, considering different initial conditions and observation constraints.

## Key Features

- **Multi-Well Simulation**: Models the proliferation dynamics of cells in multiple wells simultaneously.
- **Customizable Parameters**: Includes cell types, division times, and proliferation rules defined by empirical data and theoretical models.
- **Noise and Censorship**: Optionally incorporates observational noise and left-censorship to simulate realistic experimental conditions.

## Parameters

- **`initial_cell_type`**: The initial type of cell in each well.
- **`observation_time`**: The duration for which cell proliferation is observed.
- **`nb_wells`**: The number of wells to simulate.
- **`theta`**: A named tuple containing parameters for the simulation.
- **`empirical_T1`**: Empirical first division times for different cell types.

### Optional Arguments

- **`noise`**: Toggles the addition of observational noise.
- **`nb_gen_max`**: The maximum number of generations to observe.
- **`nb_cell_types`**: The number of different cell types in the simulation.
- **`shape_MG`**: Determines the output format for MultiGen assay data.

## Returns

- **`mat_MG`**: The proliferation data for each well, formatted according to `shape_MG`.
- **`mat_Incucyte`**: Simulated Live Cell Imaging data.
- **`vect_colony_size`**: The final size of each colony.

## Usage

This function is used for simulating the growth of cell populations under various experimental conditions. It extends the capabilities of `model_well.jl` to an entire plate of wells, providing comprehensive insights into cell dynamics at scale.



