##############
## Division ##
##############


function division_cousins((T_sister1, T_sister2)::Tuple{Float64,Float64},
		(sister1_type, sister2_type)::Tuple{Int64,Int64},
		gen::Int64, #Generation (gen=1 for initial cell)
		theta::NamedTuple, #Parameter vector 
		observation_time::Union{Float64,Int64},
		gen_type_matrix::Matrix{Int64},
		transition_matrix::Matrix{Float64},
		homogeneity_vector::Vector{Float64},
		std_div_vector::Vector{Float64},
		mu_div_vector::Vector{Float64},
		save_state::Bool,
		p::Vector{Int64},
		m::Vector{Int64},
		TA::Vector{Float64},
		T3::Vector{Float64},
		(m1,m2)::Tuple{Int64,Int64},
		nb_cell_types::Int64,
		nb_gen_max::Int64;
		limited_nb_of_gen = 20
		)
  """
  Make 4 cousin cells - recursively - differentiate and proliferate

  # Arguments required:
  - `(T_sister1, T_sister2)`: Time of division of the two sister cells
  which then correspond to the apparition time of the cousins
  - `(sister1_type, sister2_type)`: Cell types of the two sisters
  - `gen::Int64`: generation of the sisters 
  (by convention, initial cell is in generation 1)
  - `observation_time::Float64`: also end time [in hours]
  - `gen_type_matrix::Matrix{Int64}`: Store the results
  It provides the output at the observation time,
  that is, the number of cells in each generation (line)
  and of each cell type (column)

  # Arguments used only if save_state == true
  - `p::Vector{Int64}`: corresponds to the cell type of each ith cell 
  appeared in the system
  - `m::Vector{Int64}`: index of the mother of the ith cell
  - `TA::Vector{Float64}`: apparition time of the ith cell
  - `(m1,m2)::Tuple{Int64,Int64}`: index of each sister cell

  # Optionnal arguments:
  - `limited_nb_of_gen`: The simulation stops if we get too many divisions
  """

  if gen >= limited_nb_of_gen
    #Stop the simulation
    #since the nb of generations we get
    #is highly unlikely
    return nothing
  end

  ### Differentiation ###
  # First, we get 4 cells, 2 daughters per sister 
  # First sister (sis1)
  sis1_daughter1_type = sample(collect(1:nb_cell_types),
			Weights(transition_matrix[sister1_type,:]))
  sis1_daughter2_type = sample(collect(1:nb_cell_types),
			Weights(transition_matrix[sister1_type,:]))
  # Account for potential homogeneity in fate
  if rand() <= homogeneity_vector[sister1_type] 
    sis1_daughter2_type = sis1_daughter1_type
  end

  # Second sister (sis2)
  sis2_daughter1_type = sample(collect(1:nb_cell_types),
			Weights(transition_matrix[sister2_type,:]))
  sis2_daughter2_type = sample(collect(1:nb_cell_types),
			Weights(transition_matrix[sister2_type,:]))
  # Account for potential homogeneity
  if rand() <= homogeneity_vector[sister2_type] 
    sis2_daughter2_type = sis2_daughter1_type
  end


  ### Save them in the system ##
  #First, daughter cells of sister 1
  if T_sister1 <= observation_time
    gen_type_matrix[min(gen,nb_gen_max), sister1_type] -= 1
    gen_type_matrix[min(gen+1,nb_gen_max), sis1_daughter1_type] += 1
    gen_type_matrix[min(gen+1,nb_gen_max), sis1_daughter2_type] += 1

    if save_state
      push!(p, sis1_daughter1_type)
      push!(p, sis1_daughter2_type)
      push!(m, m1)
      push!(m, m1)
      m1 = length(m) #Such that index of sis1_daughter1 is now m1-1 and the one of sis1_daughter2 = m1
      push!(TA, T_sister1)
      push!(TA, T_sister1)
    end
  end

  #Then, daughter cells of sister 2
  if T_sister2 <= observation_time
    gen_type_matrix[min(gen,nb_gen_max), sister2_type] -= 1
    gen_type_matrix[min(gen+1,nb_gen_max), sis2_daughter1_type] += 1
    gen_type_matrix[min(gen+1,nb_gen_max), sis2_daughter2_type] += 1

    if save_state
      push!(p, sis2_daughter1_type)
      push!(p, sis2_daughter2_type)
      push!(m, m2)
      push!(m, m2)
      m2 = length(m) #Such that index of sis2_daughter1 is now m2-1 and the one of sis2_daughter2 = m2
      push!(TA, T_sister2)
      push!(TA, T_sister2)
    end
  end

  ### Then, make the 4 cousins proliferate ###
  cousin_types = [sis1_daughter1_type, sis1_daughter2_type, sis2_daughter1_type, sis2_daughter2_type]

  #Covariance matrix of the MV-lognormal law
  M = construct_cov_mat(cousin_types, 
			theta,
			nb_cell_types,
			std_div_vector=std_div_vector
			) #See support_function.jl

  #Parameters mu of the MV-lognormal law
  cousin_mu = [mu_div_vector[sis1_daughter1_type], 
	       mu_div_vector[sis1_daughter2_type], 
	       mu_div_vector[sis2_daughter1_type],
	       mu_div_vector[sis2_daughter2_type]]

  #Sample the quadruplet of the division time of each cousin
  (T_sis1_daughter1, 
   T_sis1_daughter2, 
   T_sis2_daughter1, 
   T_sis2_daughter2) = rand(MvLogNormal(cousin_mu,M)) .+ (T_sister1, T_sister1, T_sister2, T_sister2)

  if gen == 2
    # We store the time of third division
    T3[1] = T_sis1_daughter1
    T3[2] = T_sis1_daughter2
    T3[3] = T_sis2_daughter1
    T3[4] = T_sis2_daughter2
  end

  #Then, the cells further proliferate
  if min(T_sis1_daughter1, T_sis1_daughter2) <= observation_time

    division_cousins((T_sis1_daughter1, T_sis1_daughter2),
			(sis1_daughter1_type, sis1_daughter2_type),
			gen+1,
			theta, 
			observation_time,
			gen_type_matrix,
			transition_matrix,
			homogeneity_vector,
			std_div_vector,
			mu_div_vector,
			save_state,
			p,
			m,
			TA,
			T3,
			(m1-1,m1),
			nb_cell_types,
			nb_gen_max
			)
  end

  if min(T_sis2_daughter1, T_sis2_daughter2) <= observation_time

    division_cousins((T_sis2_daughter1, T_sis2_daughter2),
			(sis2_daughter1_type, sis2_daughter2_type),
			gen+1,
			theta, 
			observation_time,
			gen_type_matrix,
			transition_matrix,
			homogeneity_vector,
			std_div_vector,
			mu_div_vector,
			save_state,
			p,
			m,
			TA,
			T3,
			(m2-1,m2),
			nb_cell_types,
			nb_gen_max
			)
  end


end


