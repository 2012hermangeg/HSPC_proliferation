########################
### Support functions ##
########################

function construct_transition(theta::NamedTuple, nb_cell_types::Int64)
  """
  Construct the transition matrix
  that is, the probability given the mother cell type i
  that a daughter cell will be of type j: pi_j
  De-differentiation is not permitted
  such that pi_j = 0 for i > j

  # Argument
  - `theta::NamedTuple`: Parameter vector
  - `n_cell_types::Int64`: Number of cell types

  # Return
  The transition matrix
  M[i,j] is the probability that a cell will be of type j 
  given that its mother has type i
  """

  transition_matrix = zeros(Float64, nb_cell_types, nb_cell_types)

  for i in 1:nb_cell_types
    for j in 1:nb_cell_types-1

      if haskey(theta, Symbol(string("p",i,"_",j)))
      	transition_matrix[i,j] = theta[Symbol(string("p",i,"_",j))]

	      #Verification
	      if transition_matrix[i,j] < 0.0 || transition_matrix[i,j] > 1.0
	        println("error: for constructing the transition matrix, probabilities have to be between 0 and 1")
	        return nothing
	      end
      
      end

    end #Loop over columns / j

    #Summing to 1 and construction of the last transition
    transition_matrix[i,end] = 1.0 - sum(transition_matrix[i,1:end-1])

    if transition_matrix[i,end] < 0.0
      println("error: probabilities have to sum to one when constructing the transition matrix")
      return nothing
    end
      
  end #Loop over rows / i

  return transition_matrix
end



function construct_cov_mat(cell_types::Vector{Int64},
			  theta::NamedTuple,
			  nb_cell_types::Int64;
			  std_div_vector = nothing
			  )
  """
  Construction of the 4x4 covariance matrix M
  used in the MV-lognormal distribution
  for sampling the cousin division times
  depending on the concordance scenario

  # Required arguments:
  - `cell_types::Vector{Int64}`: vector
  ith value correspond to the cell type of the ith cousin cell
  Cells 1&2 are sisters
  Cells 3&4 are sisters
  Cells 1&3 and 2&4 are cousins
  - `theta::NamedTuple`: parameter vector
  - `n_cell_types::Int64`: Number of cell types

  # Optionnal arguments
  - `std_div_vector`: by default: nothing.
  In that case, will be computed from theta
  If given, corresponds to the std which appear in the covariance matrix

  Return: covariance matrix M
  """

  if length(cell_types) != 4
    println("error: `cell_types` has to have 4 elements, as many as cousins")
    return nothing
  end

  M = zeros(Float64, 4, 4) #Covariance Matrix

  if std_div_vector == nothing
    std_div_vector = [theta[Symbol(string("sig_",i))] for i in 1:nb_cell_types]
  end

  #############################################
  ### Concordance according to the scenario ###
  #############################################

  ### Between sister cells ###

  #By default, no concordance between sister cells 
  #Corresponds to C = 0
  rho_sisters_1 = 0.0 #Cor. between division time of 1st couple of sister cells
  rho_sisters_2 = 0.0 #Here, 2nd couple of sister cells

  # There might be concordance between them for some scenarios
  # And particular conditions
  # Two different concordance parameters can be considered:
  # rho_C_sisters_A and rho_C_sisters_B

  
  if theta[:C] == 0 #C -> sc. of Concordance
    #Scenario by default,
    #Nothing to do

  elseif theta[:C] == 1 #C -> sc. of Concordance
    #Same concordance between all sister cells
    #This scenario involves parameter rho_C_sisters_A
    rho_sisters_1 = theta[:rho_C_sisters_A]
    rho_sisters_2 = theta[:rho_C_sisters_A]

  elseif theta[:C] == 2
    #Concordance between sister cells only if they have the same cell type
    #Otherwise, by default, rho_sisters_i = 0.0
    #This scenario involves parameter rho_C_sisters_A
    if cell_types[1] == cell_types[2]
      rho_sisters_1 = theta[:rho_C_sisters_A]
    end

    if cell_types[3] == cell_types[4]
      rho_sisters_2 = theta[:rho_C_sisters_A]
    end

  elseif theta[:C] == 3
    #Concordance between all sister cells, 
    #but different when they have different cell types
    #This scenario involves parameters rho_C_sisters_A and rho_C_sisters_B
   
    if cell_types[1] == cell_types[2]
      rho_sisters_1 = theta[:rho_C_sisters_A]
    else
      rho_sisters_1 = theta[:rho_C_sisters_B]
    end

    if cell_types[3] == cell_types[4]
      rho_sisters_2 = theta[:rho_C_sisters_A]
    else
      rho_sisters_2 = theta[:rho_C_sisters_B]
    end
    
  elseif  theta[:C] == 4
    #Concordance between sisters 
    #except if one of them has the most mature cell type (CD34-)
    #whose index corresponds to nb_cell_types
    #In the latter, by default, rho_sisters_i = 0.0
    #This scenario involves parameter rho_C_sisters_A

    if cell_types[1] != nb_cell_types && cell_types[2] != nb_cell_types
      #nb_cell_types is also the numerical index of the most mature cells
      #since the indexation begins at 1
      rho_sisters_1 = theta[:rho_C_sisters_A]
    end

    if cell_types[3] != nb_cell_types &&  cell_types[4] != nb_cell_types
      rho_sisters_2 = theta[:rho_C_sisters_A]
    end

  elseif  theta[:C] == 5
    #Concordance between all sisters
    #Yet, different if at least one of them has the most mature type (CD34-)
    #This scenario involves parameters rho_C_sisters_A and rho_C_sisters_B

    if cell_types[1] != nb_cell_types &&  cell_types[2] != nb_cell_types
      rho_sisters_1 = theta[:rho_C_sisters_A]
    else
      rho_sisters_1 = theta[:rho_C_sisters_B]
    end

    if cell_types[3] != nb_cell_types &&  cell_types[4] != nb_cell_types
      rho_sisters_2 = theta[:rho_C_sisters_A]
    else
      rho_sisters_2 = theta[:rho_C_sisters_B]
    end
  
  else
    println("error - This concordance scenario does not exist")
  end


  ### There might be concordance between cousins ###
  #If parameters rho_C_cousins > 0
  #(=0 elsewhere)
  rho_cousins = theta[:rho_C_cousins]

  #############################################
  ### Construction of the covariance matrix ###
  #############################################
  for i in 1:4
    for j in 1:4
      M[i,j] = std_div_vector[cell_types[i]] * std_div_vector[cell_types[j]]
      if i != j
	      if (i<=2 && j<=2) #Fist couple of sister cells
	        M[i,j] *= rho_sisters_1
	      elseif (i>=3 && j>=3) #Second couple of sister cells
	        M[i,j] *= rho_sisters_2
	      else
	        #Concordance between cousins cannot be higher than between sisters
	        M[i,j] *= rho_cousins*min(rho_sisters_1,rho_sisters_2)
	      end
      end
    end
  end

  return M

end


